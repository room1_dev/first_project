﻿using ImageParser;

namespace ImageLibTest
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            string url = "https://localhost:7257/Image/tempCalc.png";
            string path = "C:\\Users\\buree\\";

            Parser parser = new Parser();

            await parser.ParseAsync(url, path);
        }
    }
}