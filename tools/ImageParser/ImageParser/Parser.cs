﻿using SixLabors.ImageSharp.Formats.Jpeg;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Xml.Linq;
using SixLabors.ImageSharp.Formats.Jpeg;
using SixLabors.ImageSharp.PixelFormats;

namespace ImageParser
{
    public class Parser
    {
        private HttpClient _sharedClient;

        public Parser()
        {
            _sharedClient = new HttpClient();
        }

        public async Task ParseAsync(string url, string path) 
        {
            if (string.IsNullOrWhiteSpace(url)) return;


            var par = await _sharedClient.GetAsync(url);

            var content = await par.Content.ReadAsStringAsync();

            var image = JsonSerializer.Deserialize<Image>(content);

            if (image == null) { return; }

            using (MemoryStream stream = new MemoryStream(image.Bytes))
            {
                var img = SixLabors.ImageSharp.Image.Load(image.Bytes);

                // Сохранить изображение в файл с форматом JPEG
                // TODO: use diff encoders
                img.Save(path + image.Name, new JpegEncoder());
            };
        }
    }
}