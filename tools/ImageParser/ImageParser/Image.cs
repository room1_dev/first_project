﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ImageParser
{
    [Serializable]
    public class Image
    {
        [JsonPropertyName("name")]
        public String Name { get; set; }
        [JsonPropertyName("bytes")]
        public byte[] Bytes { get; set; }
    }
}
