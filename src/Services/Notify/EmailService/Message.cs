﻿using System.Text;
using MimeKit;

namespace EmailService;

public class Message {
    public List<MailboxAddress> To { get; set; }
    public string Subject { get; set; }
    public string Content { get; set; }

    public IFormFileCollection Attachments { get; set; }

    public Message(IEnumerable<string> to, string subject, string content, IFormFileCollection attachments) {
        To = new List<MailboxAddress>();
        foreach (var stringItem in to) {
            var temp = stringItem.Split(' ');
            var encoding = Encoding.UTF8;
            var name = temp[0];
            var address = temp[1];
            To.Add(new MailboxAddress(encoding, name, address));
        }

        Subject = subject;
        Content = content;
        Attachments = attachments;
    }
}