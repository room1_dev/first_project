using EmailService;
using NETCore.MailKit.Extensions;
using NETCore.MailKit.Infrastructure.Internal;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddMvc();
builder.Services.AddMailKit(
    optionBuilder =>
    {
        var temp = new EmailConfiguration();
        optionBuilder.UseMailKit(new MailKitOptions() {
            //get options from sercets.json
            Server = temp.SmtpServer, // Configuration["Server"],
            Port = temp.Port, //Convert.ToInt32(Configuration["Port"]),
            SenderName = temp.From, //Configuration["SenderName"],
            SenderEmail = temp.From, //Configuration["SenderEmail"],

            // can be optional with no authentication 
            Account = temp.UserName, //Configuration["Account"],
            Password = temp.Password, //Configuration["Password"],
            // enable ssl or tls
            Security = true
        });
    });

// builder.Services.AddTransient<IEmailSender, EmailSender>();

var app = builder.Build();

// var emailConfig = builder.Configuration
//     .GetSection("EmailConfiguration")
//     .Get<EmailConfiguration>();
// builder.Services.AddSingleton(emailConfig);

builder.Services.AddControllers();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment()) {
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();