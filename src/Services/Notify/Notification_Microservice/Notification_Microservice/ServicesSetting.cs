﻿using Domain;
using Infrastructure.MailServices;
using Infrastructure.RabbitMqListener;

namespace Notification_Microservice;

public class ServicesSetting {
    public static WebApplication ServicesConfig(WebApplicationBuilder builder) {
// Custom configuration paths
        string basePath = builder.Environment.ContentRootPath;
        string appSettingsPath = Path.Combine(basePath, "Configuration", "appsettings.json");
        string appSettingsDevelopmentPath = Path.Combine(basePath, "Configuration", "appsettings.Development.json");
        string mailSettingsPath = Path.Combine(basePath, "Configuration", "MailSettings.json");
        string rabbitMqPath = Path.Combine(basePath, "Configuration", "RabbitMqSettings.json");

        builder.Configuration.AddJsonFile(appSettingsDevelopmentPath);
        builder.Configuration.AddJsonFile(mailSettingsPath);
        builder.Configuration.AddJsonFile(rabbitMqPath);

// Add services to the container.
        builder.Services.AddControllers();
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();

        //builder.Services.AddScoped<IRabbitMqListener, RabbitMqListener>();

// Configure MailSettings using custom configuration
        builder.Services.Configure<MailSettings>(builder.Configuration.GetSection(nameof(MailSettings)));
        builder.Services.AddTransient<IMailService, MailService>();
        return builder.Build();
    }
}