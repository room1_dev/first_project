using Notification_Microservice;

var builder = WebApplication.CreateBuilder(args);




var app = ServicesSetting.ServicesConfig(builder);

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment()) {
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseCors(options =>
    {
        options.AllowAnyOrigin();
        options.AllowAnyHeader();
    });
}

app.UseHttpsRedirection();

//app.UseHealthChecks("/health");
app.MapControllers();

app.Run();

