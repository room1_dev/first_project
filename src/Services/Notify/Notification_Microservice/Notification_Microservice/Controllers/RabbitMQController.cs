﻿using Domain;
using Infrastructure.MailServices;
using Infrastructure.RabbitMqListener;
using Microsoft.AspNetCore.Mvc;

namespace Notification_Microservice.Controllers {
    [ApiController]
    [Route("api/mail/[controller]")]
    public class RabbitMQController: ControllerBase {
        private readonly IMailService _mailService;
        private readonly RabbitMqListener _rabbitMqListener;
        private bool _stop = false;

        public RabbitMQController(IMailService mailService, RabbitMqListener rabbitMqListener) {
            _mailService = mailService;
            _rabbitMqListener = rabbitMqListener;
        }

        /// <summary>
        /// Starts the RabbitMQ listener asynchronously.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>A task representing the asynchronous operation.</returns>
        [HttpGet("start")]
        public async Task StartAsync(CancellationToken cancellationToken) {
            while (!_stop) {
                MailData[] mailData = await _rabbitMqListener.ExecuteAsyncRabbit(cancellationToken);
                foreach (var data in mailData) {
                    bool isEmailSent = await _mailService.SendAsync(data);
                    Console.WriteLine($"Email sent: {isEmailSent} at {DateTime.Now}");
                }
            }
        }

        /// <summary>
        /// Stops the RabbitMQ listener asynchronously.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>A task representing the asynchronous operation.</returns>
        [HttpGet("stop")]
        public async Task StopAsync(CancellationToken cancellationToken) {
            _stop = true;
            Console.WriteLine("Stopping RabbitMQ listener...");
            _rabbitMqListener.Dispose();
        }
    }
}