﻿using Domain;
using Infrastructure.MailServices;
using Infrastructure.RabbitMqListener;
using Microsoft.AspNetCore.Mvc;

namespace Notification_Microservice.Controllers;

[ApiController]
[Route("api/mail/[controller]")]
public class MailController: ControllerBase {
    private readonly IMailService _mailService;

    public MailController(IMailService mailService) {
        _mailService = mailService;
    }

    /// <summary>
    /// Sends an email asynchronously.
    /// </summary>
    /// <returns>An IActionResult indicating the success or failure of the email sending operation.</returns>
    [HttpPost("SendMailAsync")]
    public async Task<IActionResult> SendMailAsync([FromBody] MailData mailData) {
        // Retrieve the necessary data from the RabbitMqListener
        // MailData mailData = await _rabbitMqListener.ExecuteAsyncRabbit(CancellationToken.None);

        // Use the IMailService to send the email
        bool isSent = await _mailService.SendAsync(mailData);
        if (isSent) {
            return Ok();
        }
        else {
            return BadRequest("Failed to send email");
        }
    }
}