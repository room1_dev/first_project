﻿using Domain;

namespace Infrastructure.RabbitMqListener; 

public interface IRabbitMqListener {
    Task<MailData[]> ExecuteAsyncRabbit(CancellationToken stoppingToken);
    void Dispose();
}