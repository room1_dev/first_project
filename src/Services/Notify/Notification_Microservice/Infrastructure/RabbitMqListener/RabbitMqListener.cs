﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Infrastructure.RabbitMqListener
{
    public class RabbitMqListener : BackgroundService, IRabbitMqListener
    {
        private IConnection? _connection;
        private IModel? _channel;
        private readonly IConfiguration _configuration;
        private readonly string? _queue;
        private List<MailData>? _mailDataList;

        public RabbitMqListener(IConfiguration configuration)
        {
            _configuration = configuration;
            string? uri = _configuration.GetValue<string>("RabbitMqSettings:Uri");
            _queue = _configuration.GetValue<string>("RabbitMqSettings:Queue");

            if (uri == null || _queue == null)
            {
                return;
            }

            var factory = new ConnectionFactory { Uri = new Uri(uri) };
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(
                queue: _queue,
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null
            );

            _mailDataList = new List<MailData>();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();
            var consumer = new EventingBasicConsumer(_channel);
            var messageCount = _channel.MessageCount(_queue);

            for (int i = 0; i < messageCount; i++)
            {
                var ea = await Task.Run(() => _channel.BasicGet(_queue, false));

                if (ea != null)
                {
                    var content = Encoding.UTF8.GetString(ea.Body.ToArray());
                    var mailData = JsonSerializer.Deserialize<MailData>(content);

                    _mailDataList.Add(mailData); // Add the received MailData to the list

                    // Process the received message
                    Console.WriteLine($"{DateTime.Now} Received message:");
                    Console.WriteLine($"To: {mailData.To}");
                    Console.WriteLine($"From: {mailData.From}");
                    // ... process other properties

                    _channel.BasicAck(ea.DeliveryTag, false);
                }
            }
        }

        public override void Dispose()
        {
            _channel.Close();
            _connection.Close();
            base.Dispose();
        }

        public async Task<MailData[]> ExecuteAsyncRabbit(CancellationToken cancellationToken)
        {
            await ExecuteAsync(cancellationToken);
            return _mailDataList.ToArray();
        }
    }
}