﻿using MailKit.Net.Smtp;

namespace Infrastructure.Abstractions; 

public static class MailClient {
    public static SmtpClient GetSmtpClient() {
        return new SmtpClient();
    }
}