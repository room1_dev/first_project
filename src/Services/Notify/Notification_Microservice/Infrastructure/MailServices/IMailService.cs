﻿using Domain;

namespace Infrastructure.MailServices; 

public interface IMailService
{
    Task<bool> SendAsync(MailData mailData);
}