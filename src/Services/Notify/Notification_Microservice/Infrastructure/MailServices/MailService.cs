﻿using Domain;
using Infrastructure.Abstractions;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;


namespace Infrastructure.MailServices {
    public class MailService: IMailService {
        private readonly MailSettings _settings;

        public MailService(IOptions<MailSettings> settings) {
            _settings = settings.Value;
        }

        public async Task<bool> SendAsync(MailData mailData) {
            try {
                var mail = MailCreator(mailData);

                using (var client = MailClient.GetSmtpClient()) {
                    await client.ConnectAsync(_settings.Host, _settings.Port, SecureSocketOptions.SslOnConnect);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    await client.AuthenticateAsync(_settings.UserName, _settings.Password);
                    await client.SendAsync(mail);
                }
                return true;
            }
            catch (Exception e) {
                Console.WriteLine("Error: " + e.Message);
                return false;
            }
        }

        public MimeMessage MailCreator(MailData mailData) {
            // Initialize a new instance of the MimeKit.MimeMessage class
            var mail = new MimeMessage();

            GetMailData(mailData, mail);


            var body = new BodyBuilder();
            mail.Subject = mailData.Subject;
            body.HtmlBody = mailData.Body;
            mail.Body = body.ToMessageBody();


            return mail;
        }

        // **** Excample to fill in form mail ******
        
        //   "to": "edemidov88@mail.ru",
        //   "bcc": "yayaya556@yandex.ru",
        //   "cc": "yayaya556@yandex.ru",
        //   "from": "yayaya556@yandex.ru",
        //   "displayName": "eig",
        //   "replyTo": "",
        //   "replyToName": "string",
        //   "subject": "spam",
        //   "body": "spam spam"


        public void GetMailData(MailData mailData, MimeMessage mail) {
            // Sender
            mail.From.Add(new MailboxAddress(_settings.DisplayName, mailData.From ?? _settings.From));
            mail.Sender = new MailboxAddress(mailData.DisplayName ?? _settings.DisplayName,
                mailData.From ?? _settings.From);

            // Receiver
            mail.To.Add(MailboxAddress.Parse(mailData.To));

            // Set Reply to if specified in mail data
            if (!string.IsNullOrEmpty(mailData.ReplyTo))
                mail.ReplyTo.Add(new MailboxAddress(mailData.ReplyToName, mailData.ReplyTo));

            // BCC
            // Check if a BCC was supplied in the request
            if (mailData.Bcc != null) {
                // Get only addresses where value is not null or with whitespace. x = value of address
                mail.Bcc.Add(MailboxAddress.Parse(mailData.Bcc.Trim()));
            }

            // CC
            // Check if a CC address was supplied in the request
            if (mailData.Cc != null) {
                mail.Cc.Add(MailboxAddress.Parse(mailData.Cc.Trim()));
            }
        }
    }
}