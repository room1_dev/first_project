﻿using AutoFixture;
using Domain;

namespace Tests;

public class DomainTests {
    private readonly Fixture _fixture = new Fixture();


    [Fact]
    public void MailData_Should_Have_Properties()
    {
        // Arrange
        var mailData = _fixture.Create<MailData>();

        // Act

        // Assert
        Assert.NotNull(mailData.To);
        Assert.NotNull(mailData.Bcc);
        Assert.NotNull(mailData.Cc);
        Assert.NotNull(mailData.From);
        Assert.NotNull(mailData.DisplayName);
        Assert.NotNull(mailData.ReplyTo);
        Assert.NotNull(mailData.ReplyToName);
        Assert.NotNull(mailData.Subject);
        Assert.NotNull(mailData.Body);
    }
    
    [Fact]
    public void MailSettings_Should_Have_Properties()
    {
        // Arrange
        var mailSettings = _fixture.Create<MailSettings>();

        // Act

        // Assert
        Assert.NotNull(mailSettings.DisplayName);
        Assert.NotNull(mailSettings.From);
        Assert.NotNull(mailSettings.UserName);
        Assert.NotNull(mailSettings.Password);
        Assert.NotNull(mailSettings.Host);
        Assert.NotEqual(0, mailSettings.Port);
        Assert.NotNull(mailSettings.UseSsl);
        Assert.NotNull(mailSettings.UseStartTls);
        Assert.NotNull(mailSettings.UseOAuth);
    }
}