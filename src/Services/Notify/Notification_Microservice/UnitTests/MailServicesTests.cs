﻿using AutoFixture;
using Domain;
using Infrastructure.MailServices;
using MimeKit;

namespace UnitTests
{
    public class MailCreatorTests
    {
        private readonly Fixture _fixture = new Fixture();

        [Fact]
        public void MailCreator_Should_Create_MimeMessage_With_Valid_Data()
        {
            // Arrange
            var mailData = _fixture.Create<MailData>();
            var mailSettings = _fixture.Create<MailSettings>();

            // Act
            var mail = MailCreator(mailData, mailSettings);

            // Assert
            Assert.NotNull(mail);
            Assert.Equal(mailData.To, mail.To[0].ToString());
            Assert.Equal(mailData.DisplayName ?? mailSettings.DisplayName, mail.From[0].Name);
            Assert.Equal(mailData.Subject, mail.Subject);
            // Assert.Equal(mailData.Body, mail.Body.ToString());
            // Assert.Equal(mailData.From ?? mailSettings.From, mail.From[0].Name);
        }

        [Fact]
        public async Task SendAsync_Should_Return_True_On_Successful_Mail_Sending()
        {
            // Arrange
            var mailData = _fixture.Create<MailData>();
            var mailSettings = _fixture.Create<MailSettings>();
            var mail = MailCreator(mailData, mailSettings);

            // var mailSender = new MailService(mailSettings);

            // Act
            // var result = await mailSender.SendAsync(mailData);

            // Assert
            // Assert.True(result);
        }

        [Fact]
        public async Task SendAsync_Should_Return_False_On_Failed_Mail_Sending()
        {
            // Arrange
            var mailData = _fixture.Create<MailData>();
            var mailSettings = _fixture.Create<MailSettings>();
            var mail = MailCreator(mailData, mailSettings);

            // var mailSender = new MailService(mailSettings);

            // Act
            // var result = await mailSender.SendAsync(mailData);

            // Assert
            // Assert.False(result);
        }

        private MimeMessage MailCreator(MailData mailData, MailSettings mailSettings)
        {
            // Initialize a new instance of the MimeKit.MimeMessage class
            var mail = new MimeMessage();

            #region Sender / Receiver

            // Sender
            mail.From.Add(new MailboxAddress(mailData.DisplayName ?? mailSettings.DisplayName,
                mailData.From ?? mailSettings.From));
            mail.Sender = new MailboxAddress(mailData.DisplayName ?? mailSettings.DisplayName,
                mailData.From ?? mailSettings.From);

            // Receiver
            mail.To.Add(MailboxAddress.Parse(mailData.To));

            // Set Reply to if specified in mail data
            if (!string.IsNullOrEmpty(mailData.ReplyTo))
                mail.ReplyTo.Add(new MailboxAddress(mailData.ReplyToName, mailData.ReplyTo));

            // BCC
            // Check if a BCC was supplied in the request
            if (mailData.Bcc != null)
            {
                // Get only addresses where value is not null or with whitespace. x = value of address
                mail.Bcc.Add(MailboxAddress.Parse(mailData.Bcc.Trim()));
            }

            // CC
            // Check if a CC address was supplied in the request
            if (mailData.Cc != null)
            {
                mail.Cc.Add(MailboxAddress.Parse(mailData.Cc.Trim()));
            }

            #endregion

            var body = new BodyBuilder();
            mail.Subject = mailData.Subject;
            body.HtmlBody = mailData.Body;
            mail.Body = body.ToMessageBody();

            return mail;
        }
    }
}