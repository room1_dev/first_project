﻿using AutoFixture;
using Domain;
using Infrastructure.MailServices;
using Infrastructure.RabbitMqListener;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Notification_Microservice.Controllers;

namespace Tests;

public class ControllerTests {
    private readonly Fixture _fixture = new Fixture();

    [Fact]
    public async Task SendMailAsync_Should_Return_Ok_On_Successful_Email_Sending() {
        // Arrange
        var mailData = _fixture.Create<MailData>();
        var mailServiceMock = new Mock<IMailService>();
        mailServiceMock.Setup(m => m.SendAsync(It.IsAny<MailData>())).ReturnsAsync(true);

        var rabbitMqListenerMock = new Mock<IRabbitMqListener>();
        rabbitMqListenerMock.Setup(
                m => m.ExecuteAsyncRabbit(It.IsAny<CancellationToken>()))
            .ReturnsAsync(new[] { mailData });

        var controller = new MailController(mailServiceMock.Object);

        // Act
        var result = await controller.SendMailAsync(mailData);

        // Assert
        Assert.IsType<OkResult>(result);
    }

    [Fact]
    public async Task SendMailAsync_Should_Return_BadRequest_On_Failed_Email_Sending() {
        // Arrange
        var mailData = _fixture.Create<MailData>();
        var mailServiceMock = new Mock<IMailService>();
        mailServiceMock.Setup(m => m.SendAsync(It.IsAny<MailData>())).ReturnsAsync(false);

        var rabbitMqListenerMock = new Mock<IRabbitMqListener>();
        rabbitMqListenerMock.Setup(m => m.ExecuteAsyncRabbit(It.IsAny<CancellationToken>()))
            .ReturnsAsync(new[] { mailData });

        var controller = new MailController(mailServiceMock.Object);

        // Act
        var result = await controller.SendMailAsync(mailData);

        // Assert
        Assert.IsType<BadRequestObjectResult>(result);
    }

    [Fact]
    public async Task StartAsync_Should_Process_MailData_And_Send_Emails() {
        // Arrange
        var mailData = _fixture.Create<MailData[]>();
        var mailServiceMock = new Mock<IMailService>();
        mailServiceMock.Setup(m => m.SendAsync(It.IsAny<MailData>())).ReturnsAsync(true);

        var rabbitMqListenerMock = new Mock<RabbitMqListener>();
        // rabbitMqListenerMock.Setup(m => m.ExecuteAsyncRabbit(It.IsAny<CancellationToken>())).ReturnsAsync(mailData);

        // var controller = new RabbitMQController(mailServiceMock.Object, rabbitMqListenerMock.Object);
        //
        // // Act
        // await controller.StartAsync(CancellationToken.None);
        //
        // // Assert
        // foreach (var data in mailData) {
        //     mailServiceMock.Verify(m => m.SendAsync(data), Times.Once);
        // }
    }

    [Fact]
    public async Task StopAsync_Should_Dispose_RabbitMqListener() {
        // Arrange
        var mailServiceMock = new Mock<IMailService>();
        var rabbitMqListenerMock = new Mock<RabbitMqListener>();

        // var controller = new RabbitMQController(mailServiceMock.Object, rabbitMqListenerMock.Object);
        //
        // // Act
        // await controller.StopAsync(CancellationToken.None);

        // Assert
        // rabbitMqListenerMock.Verify(m => m.Dispose(), Times.Once);
    }
}