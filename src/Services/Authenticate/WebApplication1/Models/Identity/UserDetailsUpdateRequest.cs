﻿namespace Authenticate.Models.Identity
{
    public class UserDetailsUpdateRequest
    {
        public string Email { get; set; }
        public string NewEmail { get; set; }
        public string DisplayName { get; set; }
        public string NewDisplayName { get; set; }
    }
}
