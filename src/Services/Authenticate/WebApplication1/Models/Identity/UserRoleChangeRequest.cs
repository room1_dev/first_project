﻿namespace Authenticate.Models.Identity
{
    public class UserRoleChangeRequest
    {
        public string Email { get; set; }
        public List<string> Roles { get; set; }
    }
}
