﻿namespace Authenticate.Web.Models.Identity;

public class AuthResponse
{
    public Guid Id { get; set; }
    public string DisplayName { get; set; } = null!;
    public string Username { get; set; } = null!;
    public string Email { get; set; } = null!;
    public string Token { get; set; } = null!;
    public string RefreshToken { get; set; } = null!;
    public string Role { get; set; }
}