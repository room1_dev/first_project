﻿namespace Authenticate.Models.Identity
{
    public class UserRoleRequest
    {
        public string Email { get; set; }
        public string Role { get; set; }
    }
}
