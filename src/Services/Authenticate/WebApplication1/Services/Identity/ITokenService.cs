﻿using Microsoft.AspNetCore.Identity;
using Authenticate.Web.Data.Entities;

namespace Authenticate.Web.Services.Identity;

public interface ITokenService
{
    string CreateToken(ApplicationUser user, List<IdentityRole<Guid>> role);
}