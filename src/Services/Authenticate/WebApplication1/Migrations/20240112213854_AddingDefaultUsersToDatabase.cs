﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Authenticate.Migrations
{
    /// <inheritdoc />
    public partial class AddingDefaultUsersToDatabase : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "DisplayName", "RefreshToken", "RefreshTokenExpiryTime", "UserName", "NormalizedUserName", "Email", "NormalizedEmail", "EmailConfirmed", "PasswordHash", "SecurityStamp", "ConcurrencyStamp", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEnd", "LockoutEnabled", "AccessFailedCount" },
                values: new object[] { Guid.NewGuid(), "FakeAdmin", "/gG2HAM3EdfcU7PlKlKx55g+cykG9jq+5Z88Zl22U01UcDLhOlQBrrotkOW1fKSPAdv7oFxmLn6PhZ8ILB6qSA==", DateTimeOffset.Parse("2023-11-20 02:37:06.0773+03"), "FakeAdmin@mail.ru", "FAKEADMIN@MAIL.RU", "FakeAdmin@mail.ru", "FAKEADMIN@MAIL.RU", true, "AQAAAAIAAYagAAAAENvB0XMpyd9yZrXre9xFDiWvufSP9NjwdwZpiQ/aLwdF24JKvqHx4BN1hqOcGimsWg==", "JEF56QBBBZLN7RA6RMX6WHLPUKLM7L4G", "ab4e1d9f-3e4c-4e8c-9a0c-45091e3bb561", null, false, false, null, true, "0" }
            );
            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "DisplayName", "RefreshToken", "RefreshTokenExpiryTime", "UserName", "NormalizedUserName", "Email", "NormalizedEmail", "EmailConfirmed", "PasswordHash", "SecurityStamp", "ConcurrencyStamp", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEnd", "LockoutEnabled", "AccessFailedCount" },
                values: new object[] { Guid.NewGuid(), "FakeMember", "nconD8NhRJxtMYjiHDG51KfJ1IURMO3bhkyxb1a0o+1G1v7hfVxDcULfAgUghhclKQEVouN/QNJgrbJ29mDGcA==", DateTimeOffset.Parse("2023-11-20 02:37:44.362111+03"), "FakeMember@mail.ru", "FAKEMEMBER@MAIL.RU", "FakeMember@mail.ru", "FAKEMEMBER@MAIL.RU", true, "AQAAAAIAAYagAAAAEHSVzWiyXcckM6lyEEOJ6UjD23K78EJ8oPSq6UXUZO3CRsFlPg0TDOg3vBt3Mn1wXA==", "YABIOTZGBSETRLCIQBOXDCWXQTSC57P5", "b3fb0396-c59d-4134-81b3-ebeac20c60ed", null, false, false, null, true, "0" }
            );
            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { 
                    "Id",
                    "DisplayName",
                    "RefreshToken",
                    "RefreshTokenExpiryTime",
                    "UserName",
                    "NormalizedUserName",
                    "Email",
                    "NormalizedEmail", "EmailConfirmed", "PasswordHash", "SecurityStamp", "ConcurrencyStamp", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEnd", "LockoutEnabled", "AccessFailedCount" },
                values: new object[] {
                    Guid.NewGuid(),
                    "FakeModerator",
                    "JVnebzTiqV9+oNHaGoGJX5GxjpNxHSXj8dZTN2JStfIC0CXGjoMaFhiTGcr0QK3pMNB1fOs8iVMl+rSiVlGNkA==",
                    DateTimeOffset.Parse("2023-11-20 02:37:29.63444+03"), "FakeModerator@mail.ru", "FAKEMODERATOR@MAIL.RU", "FakeModerator@mail.ru", "FAKEMODERATOR@MAIL.RU", true, "AQAAAAIAAYagAAAAEELFvznZIthWoTUQIQFwBYcBaIXgGPViabF4hUJ/LtJfxL82RIlm3/jYVC8kgAXyrg==", "QUP4A4OBTHV64OP6DKSXQJRFSMEXYXA4", "f0dc7e72-bffd-4f08-9239-e70ac7c8fbe8", null, false, false, null, true, "0"
                }
            );
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
