﻿using Authenticate.Models.Identity;
using Authenticate.Web.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Authenticate.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RoleController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole<Guid>> _roleManager;

        public RoleController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole<Guid>> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        [HttpPost("assignRole")]
        public async Task<IActionResult> AssignRole(UserRoleRequest request)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);
            if (user == null)
            {
                return NotFound("User is not found");
            }

            var roleExists = await _roleManager.RoleExistsAsync(request.Role);
            if (!roleExists)
            {
                return NotFound("Role does not exist");
            }

            var isUserInRole = await _userManager.IsInRoleAsync(user, request.Role);
            if (!isUserInRole)
            {
                var result = await _userManager.AddToRoleAsync(user, request.Role);
                if (result.Succeeded)
                {
                    return Ok("The role was successfully added to the user");
                }
                else
                {
                    return BadRequest("Failed to add role to user");
                }
            }
            else
            {
                return BadRequest("The user already has this role");
            }
        }

        [HttpPost("removeRole")]
        public async Task<IActionResult> RemoveRole(UserRoleRequest request)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);
            if (user == null)
            {
                return NotFound("User is not found");
            }

            var isUserInRole = await _userManager.IsInRoleAsync(user, request.Role);
            if (isUserInRole)
            {
                var result = await _userManager.RemoveFromRoleAsync(user, request.Role);
                if (result.Succeeded)
                {
                    return Ok("The role was successfully removed from the user");
                }
                else
                {
                    return BadRequest("Failed to remove role from user");
                }
            }
            else
            {
                return BadRequest("The user does not have this role");
            }
        }
        [HttpGet("getUserRoles")]
        public async Task<IActionResult> GetUserRoles(string userEmail)
        {
            var user = await _userManager.FindByEmailAsync(userEmail);
            if (user == null)
            {
                return NotFound("User is not found");
            }

            var userRoles = await _userManager.GetRolesAsync(user);
            return Ok(userRoles);
        }
        [HttpPost("changeUserRoles")]
        public async Task<IActionResult> ChangeUserRoles(UserRoleChangeRequest request)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);
            if (user == null)
            {
                return NotFound("User not found");
            }

            var existingRoles = await _userManager.GetRolesAsync(user);

            // Удаление старых ролей
            var removeOldRolesResult = await _userManager.RemoveFromRolesAsync(user, existingRoles);
            if (!removeOldRolesResult.Succeeded)
            {
                return BadRequest("Failed to remove old roles from user");
            }

            // Добавление новых ролей
            var addNewRolesResult = await _userManager.AddToRolesAsync(user, request.Roles);
            if (!addNewRolesResult.Succeeded)
            {
                return BadRequest("Failed to add new roles to user");
            }

            return Ok("User roles changed successfully");
        }

    }
}
