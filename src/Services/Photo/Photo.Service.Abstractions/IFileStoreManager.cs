﻿namespace Photo.Service.Abstractions
{
    /// <summary>
    /// Интерфейс для хранилища фоточек
    /// </summary>
    public interface IFileStoreManager
    {
        /// <summary>
        /// Хранилище доступно?
        /// </summary>
        /// <returns></returns>
        Task<bool> IsStoreAvalableAsync();

        /// <summary>
        /// Положить картинку в хранилище
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="fileName"></param>
        /// <param name="base64string"></param>
        /// <returns></returns>
        Task PutFileAsync(Guid guid, string fileName, string base64string);

        /// <summary>
        /// Взять путь картинки из хранилища
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        string GetFilePath(Guid guid, string fileName);

        /// <summary>
        /// Удалить файл картинки.
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        Task DeleteFileAsync(Guid guid, string fileName);
    }
}
