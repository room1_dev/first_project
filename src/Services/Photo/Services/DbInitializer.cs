﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Photo.Infrastructure
{
    public class DbInitializer
    {
        public static void Initialize(EntityDbContext context)
        {
            context.Database.Migrate();
        }
    }
}
