﻿using Domain.Entities.Interfaces;
using Domain.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Photo.Infrastructure
{
    public class EntityDbContext : DbContext, IEntityDbContext
    {
        public DbSet<Album> Albums { get; set; }

        public DbSet<Image> Images { get; set; }
        public DbSet<ImagePerson> ImagePersons { get; set; }

        public EntityDbContext(DbContextOptions<EntityDbContext> options) : base(options)
        {
            Database.Migrate();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly(),
                t => t.GetInterfaces().Any(i => i.IsGenericType &&
                                          i.GetGenericTypeDefinition() == typeof(IEntityTypeConfiguration<>)));

            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        public async Task<int> SaveChangesAsync()
        {
            return await base.SaveChangesAsync();
        }
    }
}
