﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Photo.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class AddTextToImage : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ImageText",
                table: "Images",
                type: "text",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImageText",
                table: "Images");
        }
    }
}
