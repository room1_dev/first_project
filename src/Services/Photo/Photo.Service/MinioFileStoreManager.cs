﻿using Minio;
using Minio.DataModel.Args;
using Photo.Service.Abstractions;
using SettingsLib;
using static System.Net.WebRequestMethods;

namespace Photo.Service
{
    /// <summary>
    /// Реализация хранилища в MinIO
    /// На основе IFileStoreManager
    /// </summary>
    public class MinioFileStoreManager : IFileStoreManager
    {
        private readonly IMinioClient _minioClient;
        private readonly MinioSettings _minioSettings;

        public MinioFileStoreManager(IMinioClient minioClient, MinioSettings minioSettings)
        {
            _minioClient = minioClient;
            _minioSettings = minioSettings;
        }

        public string GetFilePath(Guid guid, string fileName)
        {
            string extension = Path.GetExtension(fileName);
            string objectName = $"{guid}{extension}";

            return $"{_minioSettings.ExternalEndpoint}/{_minioSettings.BucketName}/{objectName}";
        }

        public async Task<bool> IsStoreAvalableAsync()
        {
            var beArgs = new BucketExistsArgs().WithBucket(_minioSettings.BucketName);
            return await _minioClient.BucketExistsAsync(beArgs);
        }

        public async Task PutFileAsync(Guid guid, string fileName, string base64string)
        {
            string extension = Path.GetExtension(fileName);
            string objectName = $"/{guid}{extension}";
            var bytes = Convert.FromBase64String(base64string);
            var reader = new MemoryStream(bytes);
            var putObjectArgs = new PutObjectArgs()
                    .WithBucket(_minioSettings.BucketName)
                    .WithObject(objectName)
                    .WithStreamData(reader)
                    .WithContentType("image/png")
                    .WithObjectSize(reader.Length);
            await _minioClient.PutObjectAsync(putObjectArgs);
            
        }

        public async Task DeleteFileAsync(Guid guid, string fileName)
        {
            string extension = Path.GetExtension(fileName);
            string objectName = $"/{guid}{extension}";
            var removeObjectArgs = new RemoveObjectArgs()
                .WithBucket(_minioSettings.BucketName)
                .WithObject(objectName);
            await _minioClient.RemoveObjectAsync(removeObjectArgs);
        }
    }
}
