﻿using Domain.Entities.Interfaces;
using Domain.Entities.Providers;
using Domain.Entities.Providers.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Photo.Infrastructure;

namespace Photo.Service
{  
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddDAL(this IServiceCollection
        services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("EntityContext");
            services.AddDbContext<EntityDbContext>(options =>
            {
                options.UseNpgsql(connectionString);
            });
            services.AddScoped<IEntityDbContext>(provider =>
                provider.GetService<EntityDbContext>());

            services.AddScoped<IImageProvider, ImageProvider>();
            services.AddScoped<IAlbumProvider, AlbumProvider>();
            services.AddScoped<IImagePersonProvider, ImagePersonProvider>();

            return services;
        }
    }
}