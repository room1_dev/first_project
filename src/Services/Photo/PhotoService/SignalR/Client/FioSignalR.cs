﻿using Microsoft.AspNetCore.SignalR.Client;
using Photo.Api.Settings;
using Photo.Api.SignalR.Abstraction;

namespace Photo.Api.SignalR.Client
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public class FioSignalR : IFioRPC
    {
        private readonly HubConnection _connection;
        private readonly ILogger<FioSignalR> _logger;

        public FioSignalR(ILogger<FioSignalR> logger, ApplicationSettings settings)
        {
            _connection = new HubConnectionBuilder()
                .WithUrl(settings.SignalRUrl)
                .Build();

            _logger = logger;
        }

        public async Task<Dictionary<Guid, string>> Get(IEnumerable<Guid> guidPicAr)
        {
            await StartConnection("SendGuids", new List<Guid>(guidPicAr));
            
            var answerReceived = false;
            var errorReceived = false;
            var newMessage = "ничего не пришло";
            var returnedDict = new Dictionary<Guid, string>();
            _connection.On<Dictionary<Guid, string>>("ReceiveGuidsFios", (dictGuidsPaths) =>
            {
                returnedDict = dictGuidsPaths;
                answerReceived = true;
            });

            var timer = 0;
            while (!answerReceived && !errorReceived && timer < 5)
            {
                Thread.Sleep(1000);
                timer++;
            }

            if (timer >= 5)
            {
                newMessage = "превышено время ожидания";
                _logger.LogError(newMessage);
            }

            await _connection.StopAsync();

            if (!answerReceived)
            {
                _logger.LogError(newMessage);
            }

            return returnedDict;
        }

        private async Task StartConnection(string methodName, object arg)
        {
            try
            {
                await _connection.StartAsync();
                await _connection.SendAsync(methodName, arg);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error connection {ex.Message}");
                throw;
            }
        }
    }
}
