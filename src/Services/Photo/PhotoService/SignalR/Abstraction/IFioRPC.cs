﻿namespace Photo.Api.SignalR.Abstraction
{
    /// <summary>
    /// Сервис взаимодействия через Remote Procedure Call для получения ФИО по Id
    /// </summary>
    public interface IFioRPC
    {
        /// <summary>
        /// Получить ФИО по Id
        /// </summary>
        Task<Dictionary<Guid, string>> Get(IEnumerable<Guid> guidPicAr);
    }
}
