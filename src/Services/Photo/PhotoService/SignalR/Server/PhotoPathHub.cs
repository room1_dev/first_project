﻿using Domain.Entities.Providers.Interfaces;
using Microsoft.AspNetCore.SignalR;
using Photo.Service.Abstractions;

namespace Photo.Api.SignalR
{
    /// <summary>
    /// Hub для получения пути к фото
    /// </summary>
    public class PhotoPathHub : Hub
    {
        private readonly ILogger<PhotoPathHub> _logger;
        private readonly IImageProvider _imageProvider;
        private readonly IFileStoreManager _fileStoreManager;

        public PhotoPathHub(ILogger<PhotoPathHub> logger, IImageProvider imageProvider, IFileStoreManager fileStoreManager)
        {
            _logger = logger;
            _imageProvider = imageProvider;
            _fileStoreManager = fileStoreManager;
        }

        public async Task SendGuids(List<Guid> message)
        {
            var outerDict = new Dictionary<Guid, string>();
            foreach (var guid in message)
            {
                _logger.LogInformation(guid.ToString());
                var url = await GetPhotoPathInner(guid);
                if (string.IsNullOrEmpty(url))
                {
                    continue;
                }
                outerDict.Add(guid, url);
            }

            await this.Clients.All.SendAsync("ReceiveGuidsPaths", outerDict);
        }

        private async Task<string> GetPhotoPathInner(Guid guid)
        {
            bool found = await _fileStoreManager.IsStoreAvalableAsync();
            if (!found)
            {
                return string.Empty;
            }

            var img = await _imageProvider.GetImageAsync(guid);
            if (img == null)
            {
                return string.Empty;
            }
            var url = _fileStoreManager.GetFilePath(img.Id, img.Name);

            return url;
        }
    }
}
