﻿using Domain.Entities.Providers.Interfaces;
using Grpc.Core;
using GrpcService;
using Photo.Service.Abstractions;

namespace Photo.Api.Grpc
{
    public class PhotopathService : Photopath.PhotopathBase
    {
        private readonly ILogger<PhotopathService> _logger;
        private readonly IImageProvider _imageProvider;
        private readonly IFileStoreManager _fileStoreManager;

        public PhotopathService(ILogger<PhotopathService> logger, IImageProvider imageProvider, IFileStoreManager fileStoreManager)
        {
            _logger = logger;
            _imageProvider = imageProvider;
            _fileStoreManager = fileStoreManager;
        }

        public override async Task<PathReply> GetPath(GetPathRequest request, ServerCallContext context)
        {
            var hr = new PathReply();
            foreach (var guid in request.Guid)
            {
                _logger.LogInformation(guid);
                var url = await GetPhotoPathInner(guid);
                if (string.IsNullOrEmpty(url))
                {
                    continue;
                }
                hr.Answer.Add(guid, url);
            }

            return hr;
        }

        private async Task<string> GetPhotoPathInner(string id)
        {
            if (!Guid.TryParse(id, out var guid))
            {
                return string.Empty;
            }
            bool found = await _fileStoreManager.IsStoreAvalableAsync();
            if (!found)
            {
                return string.Empty;
            }

            var img = await _imageProvider.GetImageAsync(guid);
            if (img == null)
            {
                return string.Empty;
            }
            var url = _fileStoreManager.GetFilePath(img.Id, img.Name);

            return url;
        }
    }
}
