﻿namespace Photo.Api.Models
{
    public class AlbumCreateModel
    {
        public string Name { get; set; }
        public List<int> ImageIds { get; set; }
        public Guid UserId { get; set; }
    }
}
