﻿namespace Photo.Api.Models
{
    public class ImageEditModel
    {
        public string Name { get; set; }
        public string ImageText { get; set; }
    }
}
