﻿namespace Photo.Api.Models
{
    public class ImagePersonCreateModifyModel
    {
        public Guid PhotoGuid { get; set; }
        public Guid PersonGuid { get; set; }
    }
}
