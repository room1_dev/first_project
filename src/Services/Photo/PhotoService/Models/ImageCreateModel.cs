﻿namespace Photo.Api.Models
{
    public class ImageCreateModel
    {
        public string Name { get; set; }
        public string Source { get; set; }
        public Guid AlbumId { get; set; }
        public string ImageText { get; set; }
    }
}
