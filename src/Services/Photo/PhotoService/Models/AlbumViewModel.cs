﻿using Domain.Entities.Models;

namespace Photo.Api.Models
{
    public class AlbumViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public List<ImageViewModel> Images { get; set; } = new List<ImageViewModel>();

        public Guid UserId { get; set; }

        public static AlbumViewModel FromAlbum(Album album)
        {
            return new AlbumViewModel { Id = album.Id, Name = album.Name, UserId = album.UserId };
        }
    }
}
