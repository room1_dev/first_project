﻿namespace Photo.Api.Models
{
    public class ImagePersonModel
    {
        public Guid ImageId { get; set; }
        public Guid PersonId { get; set; }
        public string Fio { get; set; } = string.Empty;
    }
}
