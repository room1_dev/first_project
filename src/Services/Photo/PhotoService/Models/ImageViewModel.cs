﻿using Domain.Entities.Models;

namespace Photo.Api.Models
{
    public class ImageViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public byte[] ImageSource { get; set; } = Array.Empty<byte>();

        public string Url { get; set; } = string.Empty;

        public string ImageText { get; set; } = string.Empty;

        public static ImageViewModel FromImage(Image img)
        {
            return new ImageViewModel { Id = img.Id, Name = img.Name, ImageText = img.ImageText };
        }
    }
}
