﻿using Domain.Entities.Models;
using Domain.Entities.Providers.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Photo.Api.Models;
using Photo.Service.Abstractions;

namespace Photo.Api.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class ImageController : ControllerBase
    {
        private readonly IImageProvider _imageProvider;
        private readonly IFileStoreManager _fileStoreManager;
        private readonly ILogger _logger;


        public ImageController(IImageProvider imageProvider, IFileStoreManager fileStoreManager, ILogger<ImageController> logger)
        {
            _imageProvider = imageProvider;
            _fileStoreManager = fileStoreManager;
            _logger = logger;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ImageViewModel>> Get(Guid id)
        {
            bool found = await _fileStoreManager.IsStoreAvalableAsync();
            if (!found)
            {
                return BadRequest("Не настроено файловое хранилище");
            }

            var img =  await _imageProvider.GetImageAsync(id);
            var imgModel = ImageViewModel.FromImage(img);
            imgModel.Url = _fileStoreManager.GetFilePath(img.Id, img.Name);
            if (string.IsNullOrEmpty(imgModel.Url))
            {
                return BadRequest("Файл не найден в хранилище");
            }
            return Ok(imgModel);
        }

        [HttpPost]
        public async Task<ActionResult<Guid>> Create(ImageCreateModel imageModel)
        {
            bool found = await _fileStoreManager.IsStoreAvalableAsync();
            if (!found)
            {
                return BadRequest("Не настроено файловое хранилище");
            }

            Image image = new()
            {
                Name = imageModel.Name,
                Source = string.Empty,
                AlbumId = imageModel.AlbumId,
                ImageText = imageModel.ImageText
            };

            var id = await _imageProvider.AddImageAsync(image);
            await _fileStoreManager.PutFileAsync(id, imageModel.Name, imageModel.Source);

            return Ok(id);
        }

        [HttpPut("updateImage/{id}")]
        public async Task<IActionResult> Update(Guid id, ImageEditModel imageModel)
        {
            bool found = await _fileStoreManager.IsStoreAvalableAsync();
            if (!found)
            {
                return BadRequest("Не настроено файловое хранилище");
            }

            var img = await _imageProvider.GetImageAsync(id);
            if (img is null)
            {
                return NotFound();
            }

            img.ImageText = imageModel.ImageText;
            img.Name = imageModel.Name;

            await _imageProvider.UpdateImageAsync(img);

            return Ok();
        }

        [HttpDelete("deleteImage/{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            bool found = await _fileStoreManager.IsStoreAvalableAsync();
            if (!found)
            {
                return BadRequest("Не настроено файловое хранилище");
            }

            var img = await _imageProvider.GetImageAsync(id);
            if (img is null)
            {
                return NotFound();
            }

            await _imageProvider.DeleteImageAsync(img);
            await _fileStoreManager.DeleteFileAsync(id, img.Name);

            return Ok();
        }
    }
}
