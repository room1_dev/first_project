﻿using Domain.Entities.Providers.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Photo.Api.Models;
using Photo.Api.SignalR.Abstraction;

namespace Photo.Api.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class ImagePersonController : Controller
    {
        private readonly IImagePersonProvider _imagePersonProvider;
        private readonly ILogger _logger;
        private readonly IFioRPC _fioRpc;

        public ImagePersonController(IImagePersonProvider imagePersonProvider, ILogger<ImagePersonController> logger, IFioRPC rpc)
        {
            _imagePersonProvider = imagePersonProvider;
            _logger = logger;
            _fioRpc = rpc;
        }

        [HttpPost]
        public async Task<ActionResult<Guid>> Create(ImagePersonCreateModifyModel imagePersonModel)
        {
            await _imagePersonProvider.AddImagePersonAsync(imagePersonModel.PhotoGuid, imagePersonModel.PersonGuid);
            return Ok();
        }

        [HttpGet("{imageId}")]
        public async Task<ActionResult<List<ImagePersonModel>>> Get(Guid imageId)
        {
            var imagePerson = await _imagePersonProvider.GetImagePersonListAsync(imageId);

            var model = imagePerson
                 ?.Select(x => new ImagePersonModel { 
                    ImageId = x.ImageId, 
                    PersonId = x.PersonGuid})
                .ToList() ?? new List<ImagePersonModel>();

            if (imagePerson?.Any() ?? false)
            {
                var personGuids = imagePerson.Select(x => x.PersonGuid).ToList();
                var guidFioDict = await _fioRpc.Get(personGuids);
                if (guidFioDict?.Any() ?? false)
                {
                    model.ForEach(x => x.Fio = guidFioDict[x.PersonId]);
                }
            }

            return Ok(model);
        }

        [HttpPut]
        public async Task<IActionResult> RemoveLinkPersonPhoto(ImagePersonCreateModifyModel imagePersonModel)
        {
            var imagePerson = await _imagePersonProvider.GetImagePersonAsync(imagePersonModel.PhotoGuid, imagePersonModel.PersonGuid);
            if (imagePerson == null)
            {
                return BadRequest("Связь не найдена");
            }

            await _imagePersonProvider.DeleteImagePersonAsync(imagePerson);
            return Ok();
        }
    }
}
