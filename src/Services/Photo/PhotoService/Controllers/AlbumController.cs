﻿using Domain.Entities.Interfaces;
using Domain.Entities.Models;
using Domain.Entities.Providers.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Photo.Api.Models;
using Photo.Service.Abstractions;

namespace Photo.Api.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class AlbumController : ControllerBase
    {
        private readonly IEntityDbContext _dbContext;
        private readonly IAlbumProvider _albumProvider;
        private readonly IFileStoreManager _fileStoreManager;
        private readonly ILogger<AlbumController> _logger;


        public AlbumController(IEntityDbContext dbContext, IAlbumProvider albumProvider, IFileStoreManager fileStoreManager, ILogger<AlbumController> logger)
        {
            _dbContext = dbContext;
            _albumProvider = albumProvider;
            _fileStoreManager = fileStoreManager;
            _logger = logger;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<AlbumViewModel>> Get(Guid id)
        {
            var album = await _albumProvider.GetAlbumAsync(id);
            var albumModel = AlbumViewModel.FromAlbum(album);
            if (album.Images == null && !album.Images.Any())
            {
                return albumModel;
            }

            bool found = await _fileStoreManager.IsStoreAvalableAsync();
            if (!found)
            {
                return BadRequest("Не настроено файловое хранилище");
            }

            foreach (var img in album.Images)
            {
                var imgModel = ImageViewModel.FromImage(img);
                imgModel.Url = _fileStoreManager.GetFilePath(img.Id, img.Name);
                albumModel.Images.Add(imgModel);
            }

            return albumModel;
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var album = await _albumProvider.GetAlbumAsync(id);
            if (album is null)
            {
                return NotFound();
            }
            if (album.Images != null && album.Images.Any())
            {
                return BadRequest("Нельзя удалить альбом с картинками");
            }

            await _albumProvider.DeleteAlbumAsync(album);

            return Ok();
        }

        [HttpGet("{userId}")]
        public async Task<ActionResult<List<AlbumViewModel>>> GetAlbums(Guid userId)
        {
            var albums = await _albumProvider.GetAlbumsAsync(userId);

            var albumModels = new List<AlbumViewModel>();

            try
            {
                var found = await _fileStoreManager.IsStoreAvalableAsync();
                if (!found)
                {
                    return BadRequest("Не настроено файловое хранилище");
                }
            }
            catch (Minio.Exceptions.AccessDeniedException)
            {
                _logger.LogCritical("Нет доступа к бакету");
                throw;
            }
            
            foreach(var album in albums)
            {
                var albumModel = AlbumViewModel.FromAlbum(album);
                if (album.Images == null && !album.Images.Any())
                {
                    albumModels.Add(albumModel);
                    continue;
                }

                foreach (var img in album.Images)
                {
                    var imgModel = ImageViewModel.FromImage(img);
                    imgModel.Url = _fileStoreManager.GetFilePath(img.Id, img.Name);
                    albumModel.Images.Add(imgModel);
                }
                albumModels.Add(albumModel);

            }

            return albumModels;
        }

        [HttpPost]
        public async Task<ActionResult<Guid>> Create(AlbumCreateModel albumModel)
        {
            var images = albumModel.ImageIds.Select(id => _dbContext.Images.FindAsync(id).GetAwaiter().GetResult()).ToList();

            Album album = new Album()
            {
                Name = albumModel.Name,
                Images = images,
                UserId = albumModel.UserId
            };

            var id = await _albumProvider.AddAlbumsAsync(album);

            return id;
        }

        [HttpGet]
        public async Task<ActionResult<List<AlbumViewModel>>> GetAllAlbums()
        {
            var albums = await _albumProvider.GetAllAlbumsAsync();

            var albumModels = new List<AlbumViewModel>();
            
            foreach (var album in albums)
            {
                var albumModel = AlbumViewModel.FromAlbum(album);
                if (album.Images == null && !album.Images.Any())
                {
                    albumModels.Add(albumModel);
                    continue;
                }

                foreach (var img in album.Images)
                {
                    var imgModel = ImageViewModel.FromImage(img);
                    imgModel.Url = _fileStoreManager.GetFilePath(img.Id, img.Name);
                    albumModel.Images.Add(imgModel);
                }
                albumModels.Add(albumModel);

            }

            return albumModels;
        }
    }
}
