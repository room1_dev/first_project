using Minio;
using Photo.Api.Settings;
using Photo.Api.SignalR;
using Photo.Api.SignalR.Abstraction;
using Photo.Api.SignalR.Client;
using Photo.Infrastructure;
using Photo.Service;
using Photo.Service.Abstractions;
using SettingsLib;

var builder = WebApplication.CreateBuilder(args);

string origin = "PhotoOrigin";

builder.Services.AddSignalR();

var configuration = builder.Configuration;
var applicationSettings = configuration.Get<ApplicationSettings>();
var minioSettingsSection = configuration.GetSection("Minio");
var minioSettings = minioSettingsSection.Get<MinioSettings>()!;
builder.Services.AddMinio(config => config
    .WithEndpoint(minioSettings.Endpoint)
    .WithCredentials(minioSettings.AccessKey, minioSettings.SecretKey)
    .WithSSL(false));

builder.Services.AddSingleton(minioSettings);
builder.Services.AddTransient<IFileStoreManager, MinioFileStoreManager>();

builder.Services.AddCors();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddDAL(builder.Configuration);

builder.Services.AddCors(o => o.AddPolicy(origin, builder =>
{
    builder.AllowAnyOrigin()
        .AllowAnyMethod()
        .AllowAnyHeader();
}));

builder.Services.AddSingleton(applicationSettings);
builder.Services.AddTransient<IFioRPC, FioSignalR>();

var app = builder.Build();

using (var scope = app.Services.CreateScope())
{
    var serviceProvider = scope.ServiceProvider;

    var context =
        serviceProvider.GetRequiredService<EntityDbContext>();
    DbInitializer.Initialize(context);
}

app.UseCors(x => x
    .AllowAnyMethod()
    .AllowAnyHeader()
    .SetIsOriginAllowed(origin => true)
    .AllowCredentials());

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();
app.MapHub<PhotoPathHub>("/photopath");   // PhotoPathHub ����� ������������ ������� �� ���� /photopath

app.Run();
