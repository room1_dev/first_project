﻿using Domain.Entities.Models;

namespace Domain.Entities.Providers.Interfaces
{
    public interface IAlbumProvider
    {
        public Task<Album> GetAlbumAsync(Guid id);

        public Task DeleteAlbumAsync(Album album);

        public Task<List<Album>> GetAlbumsAsync(Guid userId);

        public Task<List<Album>> GetAllAlbumsAsync();

        public Task<Guid> AddAlbumsAsync(Album album);
    }
}