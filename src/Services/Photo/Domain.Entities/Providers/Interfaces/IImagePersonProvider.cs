﻿using Domain.Entities.Models;

namespace Domain.Entities.Providers.Interfaces
{
    /// <summary>
    /// Класс для связи картинки и персоны.
    /// </summary>
    public interface IImagePersonProvider
    {
        /// <summary>
        /// Получить связь.
        /// </summary>
        /// <param name="imageId"></param>
        /// <param name="personId"></param>
        /// <returns></returns>
        Task<List<ImagePerson>> GetImagePersonListAsync(Guid imageId);

        /// <summary>
        /// Добавить связь.
        /// </summary>
        /// <param name="imageId"></param>
        /// <param name="personId"></param>
        /// <returns></returns>
        Task AddImagePersonAsync(Guid imageId, Guid personId);

        /// <summary>
        /// Удалить связь.
        /// </summary>
        /// <returns></returns>
        Task DeleteImagePersonAsync(ImagePerson imagePerson);

        /// <summary>
        /// Получить связь.
        /// </summary>
        /// <param name="imageId"></param>
        /// <param name="personGuid"></param>
        /// <returns></returns>
        public Task<ImagePerson?> GetImagePersonAsync(Guid imageId, Guid personGuid);
    }
}
