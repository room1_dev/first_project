﻿using Domain.Entities.Models;


namespace Domain.Entities.Providers.Interfaces
{
    public interface IImageProvider
    {
        public Task<Image> GetImageAsync(Guid id);

        public Task<List<Image>> GetImagesAsync();

        public Task<Guid> AddImageAsync(Image image);

        public Task UpdateImageAsync(Image image);

        public Task DeleteImageAsync(Image image);
    }
}
