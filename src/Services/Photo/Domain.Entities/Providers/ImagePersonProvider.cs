﻿using Domain.Entities.Interfaces;
using Domain.Entities.Models;
using Domain.Entities.Providers.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Domain.Entities.Providers
{
    public class ImagePersonProvider : BaseProvider, IImagePersonProvider
    {
        public ImagePersonProvider(IEntityDbContext dbContext) : base(dbContext)
        {
        }

        public async Task AddImagePersonAsync(Guid imageId, Guid personId)
        {
            DbContext.ImagePersons.Add(new ImagePerson { ImageId = imageId, PersonGuid = personId });

            await DbContext.SaveChangesAsync();
        }

        public async Task DeleteImagePersonAsync(ImagePerson imagePerson)
        {
            DbContext.ImagePersons.Remove(imagePerson);
            await DbContext.SaveChangesAsync();
        }

        public async Task<List<ImagePerson>> GetImagePersonListAsync(Guid imageId)
        {
            return await DbContext.ImagePersons
                .Where(x => x.ImageId == imageId)
                .ToListAsync();
        }

        public async Task<ImagePerson?> GetImagePersonAsync(Guid imageId, Guid personGuid)
        {
            var imagePerson = await DbContext.ImagePersons
                .FirstOrDefaultAsync(x => x.ImageId == imageId && x.PersonGuid == personGuid);

            return imagePerson;
        }
    }
}
