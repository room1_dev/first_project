﻿using Domain.Entities.Interfaces;
using Domain.Entities.Models;
using Domain.Entities.Providers.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Providers
{
    public class ImageProvider : BaseProvider, IImageProvider
    {
        public ImageProvider(IEntityDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<Image> GetImageAsync(Guid id)
        {
            var image = await DbContext.Images
                .FirstOrDefaultAsync(img => img.Id == id);

            if(image == null)
            {
                throw new Exception("Image not found");
            }

            return image;
        }

        public async Task<List<Image>> GetImagesAsync()
        {
            return await DbContext.Images.ToListAsync();
        }

        public async Task<Guid> AddImageAsync(Image image)
        {
            DbContext.Images.Add(image);

            await DbContext.SaveChangesAsync();

            return image.Id;
        }

        public async Task UpdateImageAsync(Image image)
        {
            DbContext.Images.Update(image);
            await DbContext.SaveChangesAsync();
        }

        public async Task DeleteImageAsync(Image image)
        {
            DbContext.Images.Remove(image);
            await DbContext.SaveChangesAsync();
        }
    }
}
