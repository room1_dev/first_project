﻿using Domain.Entities.Interfaces;

namespace Domain.Entities.Providers
{
    public class BaseProvider
    {
        protected IEntityDbContext DbContext { get; set; }
        public BaseProvider(IEntityDbContext dbContext) 
        {
            DbContext = dbContext;
        }
    }
}