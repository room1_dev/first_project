﻿using Domain.Entities.Interfaces;
using Domain.Entities.Models;
using Domain.Entities.Providers.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Domain.Entities.Providers
{
    public class AlbumProvider : BaseProvider, IAlbumProvider
    {
        public AlbumProvider(IEntityDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<Album> GetAlbumAsync(Guid id)
        {
            var album = await DbContext.Albums
                .Include(album => album.Images)
                .FirstOrDefaultAsync(alb => alb.Id == id);

            if (album == null)
            {
                throw new Exception("Album not found");
            }

            return album;
        }

        public async Task<List<Album>> GetAlbumsAsync(Guid userId)
        {
            return await DbContext.Albums
                .Include(album => album.Images)
                .Where(alb => alb.UserId == userId).ToListAsync();
        }

        public async Task DeleteAlbumAsync(Album album)
        {
            DbContext.Albums.Remove(album);

            await DbContext.SaveChangesAsync();
        }

        public async Task<List<Album>> GetAllAlbumsAsync()
        {
            return await DbContext.Albums
                .Include(album => album.Images)
                .ToListAsync();
        }

        public async Task<Guid> AddAlbumsAsync(Album album)
        {
            DbContext.Albums.Add(album);

            await DbContext.SaveChangesAsync();

            return album.Id;
        }
    }
}
