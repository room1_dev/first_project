﻿namespace Domain.Entities.Models
{
    public class ImagePerson
    {
        public Guid Id { get; set; }
        public Guid ImageId { get; set; }
        public Guid PersonGuid { get; set; }
    }
}
