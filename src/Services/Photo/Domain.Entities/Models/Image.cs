﻿namespace Domain.Entities.Models
{
    public class Image
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Source { get; set; }
        public Guid AlbumId { get; set; }
        public string ImageText { get; set; }
        public List<ImagePerson> PersonsForImage { get; set; }
    }
}
