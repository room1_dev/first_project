﻿namespace Domain.Entities.Models
{
    public class Album
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<Image> Images { get; set; }

        public Guid UserId { get; set; }
    }
}
