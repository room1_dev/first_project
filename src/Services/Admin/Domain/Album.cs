﻿using Domain.Entities;

namespace Domain
{
    public class Album : IEntity<Guid>
    {
        public Guid Id { get; set; }

        /// <summary>
        /// Id Альбома с фото
        /// </summary>
        public Guid AlbumId { get; set; }

        /// <summary>
        /// Id Юзера
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Признак разрешения на просмотр альбома
        /// </summary>
        public bool WatchAlbum { get; set; }

        /// <summary>
        /// Признак разрешения на обновление альбома
        /// </summary>
        public bool UpdateAlbum { get; set; }

        /// <summary>
        /// Признак разрешения на комментирование альбома
        /// </summary>
        public bool CommentAlbum { get; set; }

        /// <summary>
        /// Признак разрешения на удаление альбома
        /// </summary>
        public bool DeleteAlbum { get; set; }

        /// <summary>
        /// Дата создания 
        /// </summary>
        public DateTime DateCreate { get; set; }

        /// <summary>
        /// Дата последнего обновления
        /// </summary>
        public DateTime DateUpdate { get; set; }

        /// <summary>
        /// Признак удаления записи
        /// </summary>
        public bool IsDeleted { get; set; }
    }
}