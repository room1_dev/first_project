﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Tree : IEntity<Guid>
    {
        public Guid Id { get; set; }

        /// <summary>
        /// Id Альбома с фото
        /// </summary>
        public Guid TreeId { get; set; }

        /// <summary>
        /// Id Юзера
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Признак разрешения на просмотр альбома
        /// </summary>
        public bool WatchTree { get; set; }

        /// <summary>
        /// Признак разрешения на обновление альбома
        /// </summary>
        public bool UpdateTree { get; set; }

        /// <summary>
        /// Признак разрешения на комментирование альбома
        /// </summary>
        public bool CommentTree { get; set; }

        /// <summary>
        /// Признак разрешения на удаление альбома
        /// </summary>
        public bool DeleteTree { get; set; }

        /// <summary>
        /// Дата создания 
        /// </summary>
        public DateTime DateCreate { get; set; }

        /// <summary>
        /// Дата последнего обновления
        /// </summary>
        public DateTime DateUpdate { get; set; }

        /// <summary>
        /// Признак удаления записи
        /// </summary>
        public bool IsDeleted { get; set; }
    }
}
