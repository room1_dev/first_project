﻿using AutoFixture;
using AutoFixture.AutoMoq;
using Domain.Entities;
using FluentAssertions;
using Moq;
using PeopleUnitTests.Helpers;
using Services.Implementations;
using Services.Repositories.Abstractions;
using System;
using Xunit;

namespace PeopleUnitTests.Services.Implementations
{
    public class PersonFamilyConnectServiceTests
    {
        private readonly Mock<IPersonRepository> _personRepositoryMock;
        private readonly Mock<IFamilyRepository> _familyRepositoryMock;
        private readonly PersonFamilyConnectService _personFamilyConnectService;

        public PersonFamilyConnectServiceTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _personRepositoryMock = fixture.Freeze<Mock<IPersonRepository>>();
            _familyRepositoryMock = fixture.Freeze<Mock<IFamilyRepository>>();
            _personFamilyConnectService = fixture.Build<PersonFamilyConnectService>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void AddPersonToFamilyAsync_FamilyIsDeleted_ReturnsFalse()
        {
            // Arrange
            var familyId = Guid.Parse("D330DE55-A37B-4878-BD7E-0F0A77B102DC");
            var family = FamilyBuilder
                .Init("IvanovbI", familyId)
                .IsDeleted()
                .Finish();
            _familyRepositoryMock.Setup(repo => repo.GetByIdAsync(familyId))
                .ReturnsAsync(family);

            var personId = Guid.NewGuid();
            var person = PersonBuilder
               .Init(firstName: "Vasiliy", innerGuid: personId)
               .Finish();
            _personRepositoryMock.Setup(repo => repo.GetByIdAsync(personId))
                .ReturnsAsync(person);

            // Act
            var res = await _personFamilyConnectService.AddPersonToFamilyAsync(familyId, personId);

            // Assert
            res.Should().BeFalse();
        }

        [Fact]
        public async void AddPersonToFamilyAsync_PersonIsDeleted_ReturnsFalse()
        {
            // Arrange
            var familyId = Guid.Parse("D330DE55-A37B-4878-BD7E-0F0A77B102DC");
            var family = FamilyBuilder
                .Init("IvanovbI", familyId)
                .Finish();
            _familyRepositoryMock.Setup(repo => repo.GetByIdAsync(familyId))
                .ReturnsAsync(family);

            var personId = Guid.Parse("D430DE55-A37B-4878-BD7E-0F0A77B102DC");
            var person = PersonBuilder
               .Init(firstName: "Vasiliy", innerGuid: personId)
               .IsDeleted()
               .Finish();
            _personRepositoryMock.Setup(repo => repo.GetByIdAsync(personId))
                .ReturnsAsync(person);

            // Act
            var res = await _personFamilyConnectService.AddPersonToFamilyAsync(familyId, personId);

            // Assert
            res.Should().BeFalse();
        }

        [Fact]
        public async void AddPersonToFamilyAsync_FamilyIsNull_ReturnsFalse()
        {
            // Arrange
            var familyId = Guid.Parse("D330DE55-A37B-4878-BD7E-0F0A77B102DC");
            Family? family = null;
            _familyRepositoryMock.Setup(repo => repo.GetByIdAsync(familyId))
                .ReturnsAsync(family);

            var personId = Guid.NewGuid();
            var person = PersonBuilder
               .Init(firstName: "Vasiliy", innerGuid: personId)
               .Finish();
            _personRepositoryMock.Setup(repo => repo.GetByIdAsync(personId))
                .ReturnsAsync(person);

            // Act
            var res = await _personFamilyConnectService.AddPersonToFamilyAsync(familyId, personId);

            // Assert
            res.Should().BeFalse();
        }

        [Fact]
        public async void AddPersonToFamilyAsync_PersonIsNull_ReturnsFalse()
        {
            // Arrange
            var familyId = Guid.Parse("D330DE55-A37B-4878-BD7E-0F0A77B102DC");
            var family = FamilyBuilder
                .Init("IvanovbI", familyId)
                .Finish();
            _familyRepositoryMock.Setup(repo => repo.GetByIdAsync(familyId))
                .ReturnsAsync(family);

            var personId = Guid.Parse("D430DE55-A37B-4878-BD7E-0F0A77B102DC");
            Person? person = null;
            _personRepositoryMock.Setup(repo => repo.GetByIdAsync(personId))
                .ReturnsAsync(person);

            // Act
            var res = await _personFamilyConnectService.AddPersonToFamilyAsync(familyId, personId);

            // Assert
            res.Should().BeFalse();
        }

        [Fact]
        public async void AddPersonToFamilyAsync_FamilyPersonsIsNull_ReturnsTrue()
        {
            // Arrange
            var familyId = Guid.Parse("D330DE55-A37B-4878-BD7E-0F0A77B102DC");
            var family = FamilyBuilder
                .Init("IvanovbI", familyId)
                .Finish();
            _familyRepositoryMock.Setup(repo => repo.GetByIdAsync(familyId))
                .ReturnsAsync(family);

            var personId = Guid.NewGuid();
            var person = PersonBuilder
               .Init(firstName: "Vasiliy", innerGuid: personId)
               .Finish();
            _personRepositoryMock.Setup(repo => repo.GetByIdAsync(personId))
                .ReturnsAsync(person);

            // Act
            var res = await _personFamilyConnectService.AddPersonToFamilyAsync(familyId, personId);

            // Assert
            res.Should().BeTrue();
            _familyRepositoryMock.Verify(moq => moq.Update(It.IsAny<Family>()));
        }

        [Fact]
        public async void AddPersonToFamilyAsync_FamilyPersonsIsNotNullAndIsNotExists_ReturnsTrue()
        {
            // Arrange
            var personId = Guid.NewGuid();
            var person = PersonBuilder
               .Init(firstName: "Vasiliy", innerGuid: personId)
               .Finish();
            _personRepositoryMock.Setup(repo => repo.GetByIdAsync(personId))
                .ReturnsAsync(person);

            var familyId = Guid.Parse("D330DE55-A37B-4878-BD7E-0F0A77B102DC");
            var family = FamilyBuilder
                .Init("IvanovbI", familyId)
                .AddPerson(Guid.NewGuid())
                .Finish();
            _familyRepositoryMock.Setup(repo => repo.GetByIdAsync(familyId))
                .ReturnsAsync(family);

            // Act
            var res = await _personFamilyConnectService.AddPersonToFamilyAsync(familyId, personId);

            // Assert
            res.Should().BeTrue();
            _familyRepositoryMock.Verify(moq => moq.Update(It.IsAny<Family>()));
        }

        [Fact]
        public async void AddPersonToFamilyAsync_FamilyPersonsIsNotNullAndExists_ReturnsTrue()
        {
            // Arrange
            var personId = Guid.NewGuid();
            var person = PersonBuilder
               .Init(firstName: "Vasiliy", innerGuid: personId)
               .Finish();
            _personRepositoryMock.Setup(repo => repo.GetByIdAsync(personId))
                .ReturnsAsync(person);

            var familyId = Guid.Parse("D330DE55-A37B-4878-BD7E-0F0A77B102DC");
            var family = FamilyBuilder
                .Init("IvanovbI", familyId)
                .AddPerson(personId)
                .Finish();
            _familyRepositoryMock.Setup(repo => repo.GetByIdAsync(familyId))
                .ReturnsAsync(family);

            // Act
            var res = await _personFamilyConnectService.AddPersonToFamilyAsync(familyId, personId);

            // Assert
            res.Should().BeFalse();
            _familyRepositoryMock.Verify(moq => moq.Update(It.IsAny<Family>()), Times.Never);
        }
    }
}
