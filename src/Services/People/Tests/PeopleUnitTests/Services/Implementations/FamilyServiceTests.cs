﻿using AutoFixture;
using AutoFixture.AutoMoq;
using AutoMapper;
using Domain.Entities;
using FluentAssertions;
using Moq;
using People_Microservice.Settings;
using PeopleUnitTests.Helpers;
using Services.Contracts.Families;
using Services.Implementations;
using Services.Repositories.Abstractions;
using System;
using System.Linq;
using Xunit;

namespace PeopleUnitTests.Services.Implementations
{
    public class FamilyServiceTests
    {
        private readonly Mock<IPersonRepository> _personRepositoryMock;
        private readonly Mock<IFamilyRepository> _familyRepositoryMock;
        private readonly FamilyService _familyService;

        public FamilyServiceTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _personRepositoryMock = fixture.Freeze<Mock<IPersonRepository>>();
            _familyRepositoryMock = fixture.Freeze<Mock<IFamilyRepository>>();
            fixture.Register<IMapper>(() => new Mapper(MapperSettings.GetMapperConfiguration()));
            _familyService = fixture.Build<FamilyService>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void GetByIdAsync_FamilyIsNotFound_ReturnsNull()
        {
            // Arrange
            var familyId = Guid.Parse("894645DD-76B2-4DB5-8292-955445AF50B4");
            Family? family = null;

            _familyRepositoryMock.Setup(repo => repo.GetByIdAsync(familyId))
                .ReturnsAsync(family);

            // Act
            var familyDTO = await _familyService.GetByIdAsync(familyId);

            // Assert
            familyDTO.Should().BeNull();
        }

        [Fact]
        public async void GetByIdAsync_FamilyWithPerson_ReturnsFamilyDTOWithoutPerson()
        {
            // Arrange
            var family = FamilyBuilder
                .Init("IvanovbI")
                .Finish();
            _familyRepositoryMock.Setup(repo => repo.GetByIdAsync(family.Id))
                .ReturnsAsync(family);

            // Act
            var familyDTO = await _familyService.GetByIdAsync(family.Id);

            // Assert
            familyDTO.Should().NotBeNull();
            familyDTO.Name.Should().Be(family.Name);
        }

        [Fact]
        public async void GetByIdAsync_FamilyWithPerson_ReturnsFamilyDTOWithPerson()
        {
            // Arrange
            var family = FamilyBuilder
                .Init("IvanovbI")
                .AddPerson()
                .Finish();
            _familyRepositoryMock.Setup(repo => repo.GetByIdAsync(family.Id))
                .ReturnsAsync(family);

            var person = new Person { FirstName = "Vasiliy" };
            _personRepositoryMock.Setup(repo => repo.GetByIdAsync(family.FamilyPersons.First().PersonId))
                .ReturnsAsync(person);

            // Act
            var familyDTO = await _familyService.GetByIdAsync(family.Id);

            // Assert
            familyDTO.Should().NotBeNull();
            familyDTO.Name.Should().Be(family.Name);
            familyDTO.PersonList.Should().NotBeNull();
            familyDTO.PersonList.First().FirstName.Should().Be(person.FirstName);
        }

        [Fact]
        public async void Update_FamilyIsNotFound_ReturnsFalse()
        {
            // Arrange
            var familyId = Guid.Parse("894645DD-76B2-4DB5-8292-955445AF50B4");
            Family? family = null;

            _familyRepositoryMock.Setup(repo => repo.GetByIdAsync(familyId))
                .ReturnsAsync(family);

            // Act
            var res = await _familyService.Update(familyId, new CreateUpdateFamilyDTO());

            // Assert
            res.Should().Be(false);
        }

        [Fact]
        public async void Update_FamilyIsFound_ReturnsTrue()
        {
            // Arrange
            var family = FamilyBuilder
                .Init("IvanovbI")
                .Finish();
            _familyRepositoryMock.Setup(repo => repo.GetByIdAsync(family.Id))
                .ReturnsAsync(family);

            // Act
            var res = await _familyService.Update(family.Id, new CreateUpdateFamilyDTO());

            // Assert
            res.Should().Be(true);
        }

        [Fact]
        public async void Delete_FamilyIsNotFound_ReturnsFalse()
        {
            // Arrange
            var familyId = Guid.Parse("894645DD-76B2-4DB5-8292-955445AF50B4");
            Family? family = null;

            _familyRepositoryMock.Setup(repo => repo.GetByIdAsync(familyId))
                .ReturnsAsync(family);

            // Act
            var res = await _familyService.Delete(familyId);

            // Assert
            res.Should().Be(false);
        }

        [Fact]
        public async void Delete_FamilyIsFound_ReturnsTrue()
        {
            // Arrange
            var family = FamilyBuilder
                .Init("IvanovbI")
                .Finish();
            _familyRepositoryMock.Setup(repo => repo.GetByIdAsync(family.Id))
                .ReturnsAsync(family);

            // Act
            var res = await _familyService.Delete(family.Id);

            // Assert
            res.Should().Be(true);
        }
    }
}