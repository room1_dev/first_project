﻿using AutoFixture;
using AutoFixture.AutoMoq;
using AutoMapper;
using Domain.Entities;
using FluentAssertions;
using Moq;
using People_Microservice.Settings;
using PeopleUnitTests.Helpers;
using Services.Contracts.Persons;
using Services.Implementations;
using Services.Repositories.Abstractions;
using System;
using System.Linq;
using Xunit;


namespace PeopleUnitTests.Services.Implementations
{
    public class PersonServiceTests
    {
        private readonly Mock<IPersonRepository> _personRepositoryMock;
        private readonly Mock<IFamilyRepository> _familyRepositoryMock;
        private readonly PersonService _personService;

        public PersonServiceTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _personRepositoryMock = fixture.Freeze<Mock<IPersonRepository>>();
            _familyRepositoryMock = fixture.Freeze<Mock<IFamilyRepository>>();
            fixture.Register<IMapper>(() => new Mapper(MapperSettings.GetMapperConfiguration()));
            _personService = fixture.Build<PersonService>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void GetByIdAsync_PersonIsNotFound_ReturnsNull()
        {
            // Arrange
            var personId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Person? person = null;

            _personRepositoryMock.Setup(repo => repo.GetByIdAsync(personId))
                .ReturnsAsync(person);

            // Act
            var personDTO = await _personService.GetByIdAsync(personId);

            // Assert
            personDTO.Should().BeNull();
        }

        [Fact]
        public async void GetByIdAsync_PersonWithFamily_ReturnsPersonDTOWithoutFamily()
        {
            // Arrange
            var person = PersonBuilder
                .Init("Vasiliy")
                .Finish();
            _personRepositoryMock.Setup(repo => repo.GetByIdAsync(person.Id))
                .ReturnsAsync(person);

            // Act
            var personDTO = await _personService.GetByIdAsync(person.Id);

            // Assert
            personDTO.Should().NotBeNull();
            personDTO.FirstName.Should().Be(person.FirstName);
        }

        [Fact]
        public async void GetByIdAsync_PersonWithFamily_ReturnsPersonDTOWithFamily()
        {
            // Arrange
            var person = PersonBuilder
               .Init("Vasiliy")
               .AddFamily()
               .Finish();
            _personRepositoryMock.Setup(repo => repo.GetByIdAsync(person.Id))
                .ReturnsAsync(person);

            var family = new Family { Name = "IvanovbI" };
            _familyRepositoryMock.Setup(repo => repo.GetByIdAsync(person.FamilyPersons.First().FamilyId))
                .ReturnsAsync(family);

            // Act
            var personDTO = await _personService.GetByIdAsync(person.Id);

            // Assert
            personDTO.Should().NotBeNull();
            personDTO.FirstName.Should().Be(person.FirstName);
            personDTO.Families.Should().NotBeNull();
            personDTO.Families.First().Name.Should().Be(family.Name);
        }

        [Fact]
        public async void Update_PersonIsNotFound_ReturnsFalse()
        {
            // Arrange
            var personId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Person? person = null;

            _personRepositoryMock.Setup(repo => repo.GetByIdAsync(personId))
                .ReturnsAsync(person);

            // Act
            var res = await _personService.Update(personId, new CreateUpdatePersonDTO());

            // Assert
            res.Should().Be(false);
        }

        [Fact]
        public async void Update_PersonIsFound_ReturnsTrue()
        {
            // Arrange
            var person = PersonBuilder
                .Init("Vasiliy")
                .Finish();
            _personRepositoryMock.Setup(repo => repo.GetByIdAsync(person.Id))
                .ReturnsAsync(person);

            // Act
            var res = await _personService.Update(person.Id, new CreateUpdatePersonDTO());

            // Assert
            res.Should().Be(true);
        }

        [Fact]
        public async void Delete_PersonIsNotFound_ReturnsFalse()
        {
            // Arrange
            var personId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Person? person = null;

            _personRepositoryMock.Setup(repo => repo.GetByIdAsync(personId))
                .ReturnsAsync(person);

            // Act
            var res = await _personService.Delete(personId);

            // Assert
            res.Should().Be(false);
        }

        [Fact]
        public async void Delete_PersonIsFound_ReturnsTrue()
        {
            // Arrange
            var person = PersonBuilder
                .Init("Vasiliy")
                .Finish();
            _personRepositoryMock.Setup(repo => repo.GetByIdAsync(person.Id))
                .ReturnsAsync(person);

            _personRepositoryMock.Setup(repo => repo.GetByIdAsync(person.Id))
                .ReturnsAsync(person);

            // Act
            var res = await _personService.Delete(person.Id);

            // Assert
            res.Should().Be(true);
        }
    }
}
