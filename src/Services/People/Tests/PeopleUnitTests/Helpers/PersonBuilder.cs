﻿using Domain.Entities;
using System;
using System.Collections.Generic;

namespace PeopleUnitTests.Helpers
{
    public class PersonBuilder
    {
        private PersonBuilder() { }

        private readonly Person _person = new Person();

        public static PersonBuilder Init(string firstName = "", string lastName = "", Guid? innerGuid = null)
        {
            if (innerGuid == null)
            {
                innerGuid = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            }
            var builder = new PersonBuilder();
            builder._person.Id = innerGuid.Value;
            builder._person.FirstName = firstName;
            builder._person.LastName = lastName;
            return new PersonBuilder();
        }

        public PersonBuilder AddFamily()
        {
            var familyId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b166");
            _person.FamilyPersons = new List<FamilyPerson>()
            {
                new FamilyPerson
                {
                    PersonId = _person.Id,
                    FamilyId = familyId
                }
            };
            return this;
        }

        public PersonBuilder IsDeleted()
        {
            _person.IsDeleted = true;
            return this;
        }

        public Person Finish()
        {
            return _person;
        }
    }
}
