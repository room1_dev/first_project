﻿using Domain.Entities;
using System;
using System.Collections.Generic;

namespace PeopleUnitTests.Helpers
{
    public class FamilyBuilder
    {
        private FamilyBuilder() { }

        private readonly Family _family = new Family();

        public static FamilyBuilder Init(string name = "", Guid? innerGuid = null)
        {
            if (innerGuid == null)
            {
                innerGuid = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b166");
            }
            var builder = new FamilyBuilder();
            builder._family.Id = innerGuid.Value;
            builder._family.Name = name;
            return new FamilyBuilder();
        }

        public FamilyBuilder AddPerson()
        {
            var personID = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b167");
            _family.FamilyPersons = new List<FamilyPerson>()
            {
                new FamilyPerson
                {
                    PersonId = personID,
                    FamilyId = _family.Id
                }
            };
            return this;
        }

        public FamilyBuilder IsDeleted()
        {
            _family.IsDeleted = true;
            return this;
        }

        public FamilyBuilder AddPerson(Guid? innerPersonGuid = null)
        {
            if (innerPersonGuid == null)
            {
                innerPersonGuid = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b166");
            }
            _family.FamilyPersons = new List<FamilyPerson>()
            {
                new FamilyPerson
                {
                    PersonId = innerPersonGuid.Value,
                    FamilyId = _family.Id
                }
            };
            return this;
        }

        public Family Finish()
        {
            return _family;
        }
    }
}
