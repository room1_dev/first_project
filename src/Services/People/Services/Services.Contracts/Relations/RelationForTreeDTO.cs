﻿namespace Services.Contracts.Relations
{
    public class RelationForTreeDTO
    {
        public Guid PersonGuid { get; set; }

        public string Name { get; set; } = string.Empty;

        public int Gender { get; set; }

        public DateTime DateOfBirth { get; set; }

        public RelationRequestDTO[] Parents { get; set; } = Array.Empty<RelationRequestDTO>();

        public RelationRequestDTO[] Siblings { get; set; } = Array.Empty<RelationRequestDTO>();

        public RelationRequestDTO[] Spouses { get; set; } = Array.Empty<RelationRequestDTO>();

        public RelationRequestDTO[] Children { get; set; } = Array.Empty<RelationRequestDTO>();

        /// <summary>
        /// ID картинки для персоны
        /// </summary>
        public Guid? ImageId { get; set; } = null;
    }
}
