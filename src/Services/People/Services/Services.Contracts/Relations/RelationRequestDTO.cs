﻿namespace Services.Contracts.Relations
{
    public class RelationRequestDTO
    {
        public Guid PersonGuid { get; set; }
        public string PersonName { get; set; } = string.Empty;
    }
}
