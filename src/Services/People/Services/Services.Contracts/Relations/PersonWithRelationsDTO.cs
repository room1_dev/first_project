﻿namespace Services.Contracts.Relations
{
    public class PersonWithRelationsDTO
    {
        public Guid PersonGuid {get; set;}
        public string PersonName { get; set;} = string.Empty;

        public IList<PersonWithRelationsSubDTO> RelationPersons { get; set; } = new List<PersonWithRelationsSubDTO>();

    }
}
