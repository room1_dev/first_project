﻿namespace Services.Contracts.Relations
{
    public class PersonWithRelationsSubDTO
    {
        public Guid PersonGuid { get; set; }
        public string PersonName { get; set; } = string.Empty;

        public string RelationType { get; set; } = string.Empty;
    }
}
