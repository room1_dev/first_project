﻿namespace Services.Contracts.Relations
{
    public class CreateRelationDTO
    {
        public Guid PersonSourceGuid { get; set; }
        public Guid PersonDestGuid { get; set; }
        public string RelationTypeName { get; set; } = string.Empty;
    }
}
