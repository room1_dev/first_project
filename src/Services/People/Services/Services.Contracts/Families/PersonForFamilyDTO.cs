﻿namespace Services.Contracts.Families
{
    public class PersonForFamilyDTO
    {
        /// <summary>
        /// Id.
        /// </summary>
        public Guid Id { get; set; }

        public string FirstName { get; set; } = string.Empty;
        public string MiddleName { get; set; } = string.Empty;
        public string LastName { get; set; } = string.Empty;
        public DateTime DateOfBirth { get; set; }
        public DateTime? DateOfDeath { get; set; } = null;
    }
}
