﻿namespace Services.Contracts.Families
{
    public class FamilyDTO
    {
        /// <summary>
        /// Id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название семьи
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Описание семьи
        /// </summary>
        public string Description { get; set; } = string.Empty;

        /// <summary>
        /// Список персон в семье
        /// </summary>
        public List<PersonForFamilyDTO> PersonList { get; set; } = new List<PersonForFamilyDTO>();

        public List<Guid> UserList { get; set; } = new List<Guid>();
    }
}
