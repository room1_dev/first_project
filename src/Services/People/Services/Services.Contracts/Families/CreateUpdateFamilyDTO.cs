﻿namespace Services.Contracts.Families
{
    public class CreateUpdateFamilyDTO
    {
        /// <summary>
        /// Название семьи
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Юзер, который создал, сделал обновление
        /// </summary>
        public Guid UserID { get; set; }

        /// <summary>
        /// Описание семьи
        /// </summary>
        public string Description { get; set; } = string.Empty;
    }
}