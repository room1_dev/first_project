﻿namespace Services.Contracts.Persons
{
    public class FamilyForPersonDTO
    {
        /// <summary>
        /// Id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название.
        /// </summary>
        public string Name { get; set; } = string.Empty;
    }
}
