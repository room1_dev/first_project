﻿namespace Services.Contracts.Persons
{
    /// <summary>
    /// ДТО персоны
    /// </summary>
    public class CreateUpdatePersonDTO
    {
        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; } = string.Empty;

        /// <summary>
        /// Отчество
        /// </summary>
        public string MiddleName { get; set; } = string.Empty;

        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; } = string.Empty;

        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// Дата смерти, если человек жив = null
        /// </summary>
        public DateTime? DateOfDeath { get; set; } = null;

        /// <summary>
        /// Пол
        /// </summary>
        public int Gender { get; set; }

        /// <summary>
        /// Описание для персоны
        /// </summary>
        public string Description { get; set; } = string.Empty;

        /// <summary>
        /// ID картинки для персоны
        /// </summary>
        public Guid? ImageId { get; set; } = null;
    }
}
