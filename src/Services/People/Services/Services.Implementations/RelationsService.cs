﻿using Domain.Entities;
using Services.Abstractions;
using Services.Contracts.Relations;
using Services.Repositories.Abstractions;

namespace Services.Implementations
{
    /// <summary>
    /// <inheritdoc cref="IRelationsService"/>
    /// </summary>
    public class RelationsService : IRelationsService
    {
        private readonly IPersonRepository _personRepository;
        private readonly IRelationshipRepository _relationshipRepository;
        private readonly IRelationshipTypeRepository _relationshipTypeRepository;
        private readonly IFamilyRepository _familyRepository;

        public RelationsService(
            IPersonRepository personRepository, 
            IRelationshipRepository relationshipRepository, 
            IRelationshipTypeRepository relationshipTypeRepository,
            IFamilyRepository familyRepository)
        {
            _personRepository = personRepository;
            _relationshipRepository = relationshipRepository;
            _relationshipTypeRepository = relationshipTypeRepository;
            _familyRepository = familyRepository;
        }

        /// <summary>
        /// <inheritdoc cref="IRelationsService.GetPersonWithStrongRelations(Guid)"/>
        /// </summary>
        public async Task<PersonWithRelationsDTO?> GetPersonWithStrongRelations(Guid guid)
        {
            var person = await _personRepository.GetByIdAsync(guid);
            if (person == null || person.IsDeleted)
            {
                return null;
            }

            var returnRelationsPerson = new PersonWithRelationsDTO()
            {
                PersonGuid = guid,
                PersonName = $"{person.LastName} {person.FirstName} {person.MiddleName}",
                RelationPersons = new List<PersonWithRelationsSubDTO>()
            };

            var relationshipList = await _relationshipRepository.GetByPersonGuidAsync(guid);
            if (!relationshipList.Any())
            {
                return returnRelationsPerson;
            }

            foreach (var relation in relationshipList)
            {
                if (relation.PersonDest == null || relation.RelationshipType == null)
                {
                    throw new InvalidDataException("Проблема с данными в связях членов семей");
                }
                returnRelationsPerson.RelationPersons.Add(new PersonWithRelationsSubDTO
                {
                    PersonGuid = relation.PersonDest.Id,
                    PersonName = $"{relation.PersonDest.LastName} {relation.PersonDest.FirstName} {relation.PersonDest.MiddleName}",
                    RelationType = relation.RelationshipType.Type
                });
            }

            return returnRelationsPerson;
        }

        /// <summary>
        /// <inheritdoc cref="IRelationsService.GetFamilyRelationsForGenTree(Guid)"/>
        /// </summary>
        public async Task<List<RelationForTreeDTO>> GetFamilyRelationsForGenTree(Guid familyGuid)
        {
            var familyWithPersons = await _familyRepository.GetByIdAsync(familyGuid);
            if (familyWithPersons == null || !familyWithPersons.FamilyPersons.Any())
            {
                return new List<RelationForTreeDTO>();
            }

            var retList = new List<RelationForTreeDTO>();
            foreach(var personFromFamily in familyWithPersons.FamilyPersons)
            {
                var relationModel = await GetPersonRelationsForGenTree(personFromFamily.PersonId);
                if (relationModel == null)
                {
                    continue;
                }
                retList.Add(relationModel);
            }
           
            return retList.OrderBy(p => p.DateOfBirth).ToList();
        }

        public async Task<RelationForTreeDTO?> GetPersonRelationsForGenTree(Guid personID)
        {
            var person = await _personRepository.GetByIdAsync(personID);
            if (person == null || person.IsDeleted)
            {
                return null;
            }

            var relationModel = new RelationForTreeDTO()
            {
                PersonGuid = personID,
                Name = $"{person.LastName} {person.FirstName} {person.MiddleName}",
                DateOfBirth = person.DateOfBirth,
                ImageId = person.ImageId
            };

            var relationshipList = await _relationshipRepository.GetByPersonGuidAsync(personID, true);
            if (!relationshipList.Any())
            {
                return relationModel;
            }

            relationModel.Parents = relationshipList
                .Where(w => w.RelationshipType.Type == "Parent")
                .Select(s => new RelationRequestDTO { PersonGuid = s.PersonDestId, PersonName = $"{s.PersonDest.LastName} {s.PersonDest.FirstName} {s.PersonDest.MiddleName}" }) 
                .ToArray();

            relationModel.Children = relationshipList
                .Where(w => w.RelationshipType.Type == "Child")
                .Select(s => new RelationRequestDTO { PersonGuid = s.PersonDestId, PersonName = $"{s.PersonDest.LastName} {s.PersonDest.FirstName} {s.PersonDest.MiddleName}" })
                .ToArray();

            relationModel.Siblings = relationshipList
                .Where(w => w.RelationshipType.Type == "Sibling")
                .Select(s => new RelationRequestDTO { PersonGuid = s.PersonDestId, PersonName = $"{s.PersonDest.LastName} {s.PersonDest.FirstName} {s.PersonDest.MiddleName}" })
                .ToArray();

            relationModel.Spouses = relationshipList
                .Where(w => w.RelationshipType.Type == "Spouse")
                .Select(s => new RelationRequestDTO { PersonGuid = s.PersonDestId, PersonName = $"{s.PersonDest.LastName} {s.PersonDest.FirstName} {s.PersonDest.MiddleName}" })
                .ToArray();

            return relationModel;
        }

        /// <summary>
        /// <inheritdoc cref="IRelationsService.Create(CreateRelationDTO)"/>
        /// </summary>
        public async Task<Guid?> Create(CreateRelationDTO createRelationDto)
        {
            // получение типа связи по имени
            var type = await _relationshipTypeRepository.GetByTypeName(createRelationDto.RelationTypeName);
            if (type == null)
            {
                return null;
            }

            Relationship? returnRelation = null; 
            var relationGuid = await _relationshipRepository.GetRelationshipAsync(createRelationDto.PersonSourceGuid, createRelationDto.PersonDestGuid, type.Id);
            if (relationGuid == null)
            {
                // создание прямой связи
                var newStraightRelation = new Relationship
                {
                    PersonSourceId = createRelationDto.PersonSourceGuid,
                    PersonDestId = createRelationDto.PersonDestGuid,
                    RelationshipTypeId = type.Id
                };
                returnRelation = await _relationshipRepository.AddAsync(newStraightRelation);
            }

            var relationMirrorGuid = await _relationshipRepository.GetRelationshipAsync(createRelationDto.PersonSourceGuid, createRelationDto.PersonDestGuid, type.Id);
            if (relationMirrorGuid == null)
            {
                // создание обратной связи
                var newMirorRelation = new Relationship
                {
                    PersonSourceId = createRelationDto.PersonDestGuid,
                    PersonDestId = createRelationDto.PersonSourceGuid,
                    RelationshipTypeId = type.ReverseRelationshipTypeId
                };
                await _relationshipRepository.AddAsync(newMirorRelation);
            }

            // сохранение в БД
            if (relationGuid == null || relationMirrorGuid == null)
            {
                await _relationshipRepository.SaveChangesAsync();
            }
            return returnRelation?.Id;
        }
    }
}
