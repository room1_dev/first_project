﻿using AutoMapper;
using Domain.Entities;
using Services.Abstractions;
using Services.Contracts.Families;
using Services.Repositories.Abstractions;

namespace Services.Implementations
{
    /// <summary>
    /// <inheritdoc cref="IFamilyService"/>
    /// </summary>
    public class FamilyService : IFamilyService
    {
        private readonly IFamilyRepository _familyRepository;
        private readonly IPersonRepository _personRepository;
        private readonly IMapper _mapper;

        public FamilyService(IFamilyRepository familyRepository, IPersonRepository personRepository, IMapper mapper)
        {
            _familyRepository = familyRepository;
            _personRepository = personRepository;
            _mapper = mapper;
        }

        /// <inheritdoc cref="IFamilyService.Create(Contracts.Persons.CreateUpdatePersonDTO)"/>
        public async Task<Guid?> Create(CreateUpdateFamilyDTO createUpdateFamilyDTO)
        {
            var familyToCreate = _mapper.Map<CreateUpdateFamilyDTO, Family>(createUpdateFamilyDTO);
            var nowDateTime = DateTime.UtcNow;
            familyToCreate.Created = nowDateTime;
            familyToCreate.LastUpdated = nowDateTime;
            familyToCreate.IsDeleted = false;
           
            var createdPerson = await _familyRepository.AddAsync(familyToCreate);
            await _familyRepository.SaveChangesAsync();

            var family = await _familyRepository.GetByIdAsync(createdPerson.Id);
            if (family != null)
            {
                family.FamilyUsers = new List<FamilyUser> { 
                    new FamilyUser 
                    { 
                        FamilyId = family.Id,
                        UserId = createUpdateFamilyDTO.UserID
                    } 
                };
                _familyRepository.Update(family);
                await _familyRepository.SaveChangesAsync();
            }
            
            return createdPerson?.Id;
        }

        /// <inheritdoc cref="IFamilyService.Delete(Guid)"/>
        public async Task<bool> Delete(Guid id)
        {
            var family = await _familyRepository.GetByIdAsync(id);
            if (family == null || family.IsDeleted)
            {
                return false;
            }

            family.IsDeleted = true;
            family.LastUpdated = DateTime.UtcNow;
            _familyRepository.Update(family);
            await _familyRepository.SaveChangesAsync();

            return true;
        }

        /// <inheritdoc cref="IFamilyService.GetByIdAsync(Guid)"/>
        public async Task<FamilyDTO?> GetByIdAsync(Guid id)
        {
            var family = await _familyRepository.GetByIdAsync(id);
            if (family == null)
            {
                return null;
            }

            var familyDTO = _mapper.Map<Family, FamilyDTO>(family);
            
            if (family.FamilyPersons != null && family.FamilyPersons.Any())
            {
                var personList = new List<PersonForFamilyDTO>();
                foreach (var familyPersonsID in family.FamilyPersons)
                {
                    var person = await _personRepository.GetByIdAsync(familyPersonsID.PersonId);
                    if (person != null && !person.IsDeleted)
                    {
                        personList.Add(
                            new PersonForFamilyDTO { 
                                Id = person.Id, 
                                FirstName = person.FirstName, 
                                MiddleName = person.MiddleName, 
                                LastName = person.LastName, 
                                DateOfBirth = person.DateOfBirth,
                                DateOfDeath = person.DateOfDeath
                            });
                    }
                }
                familyDTO.PersonList = personList;
            }

            if (family.FamilyUsers.Any())
            {
                foreach (var user in family.FamilyUsers)
                {
                    familyDTO.UserList.Add(user.UserId);
                }
            }

            return familyDTO;
        }

        /// <inheritdoc cref="IFamilyService.GetAllAsync()"/>
        public async Task<List<FamilyDTO>> GetAllAsync()
        {
            var familyList = await _familyRepository.GetAllAsync();
            if (familyList == null || !familyList.Any())
            {
                return new List<FamilyDTO>();
            }

            return familyList.Where(f => !f.IsDeleted).Select(f => _mapper.Map<Family, FamilyDTO>(f)).ToList();
        }

        /// <inheritdoc cref="IFamilyService.Update(Guid, CreateUpdateFamilyDTO)"/>
        public async Task<bool> Update(Guid id, CreateUpdateFamilyDTO createUpdateFamilyDTO)
        {
            var family = await _familyRepository.GetByIdAsync(id);
            if (family == null || family.IsDeleted)
            {
                return false;
            }

            family.Name = createUpdateFamilyDTO.Name;
            family.Description = createUpdateFamilyDTO.Description;
            family.LastUpdated = DateTime.UtcNow;
            _familyRepository.Update(family);
            await _familyRepository.SaveChangesAsync();

            return true;
        }

        public async Task<List<FamilyDTO>> GetByUserIdAsync(Guid userId)
        {
            var familyList = await _familyRepository.GetByUserId(userId);
            if (!familyList.Any())
            {
                return new List<FamilyDTO>();
            }

            return familyList.Where(f => !f.IsDeleted).Select(f => _mapper.Map<Family, FamilyDTO>(f)).ToList();
        }
    }
}
