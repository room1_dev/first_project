﻿using Domain.Entities;
using Services.Abstractions;
using Services.Repositories.Abstractions;

namespace Services.Implementations
{
    /// <summary>
    /// <inheritdoc cref="IPersonFamilyConnectService"/>
    /// </summary>
    public class PersonFamilyConnectService : IPersonFamilyConnectService
    {
        private readonly IFamilyRepository _familyRepository;
        private readonly IPersonRepository _personRepository;

        public PersonFamilyConnectService(IFamilyRepository familyRepository, IPersonRepository personRepository)
        {
            _familyRepository = familyRepository;
            _personRepository = personRepository;
        }

        /// <inheritdoc cref="IPersonFamilyConnectService.AddPersonToFamilyAsync(Guid, Guid)"/>
        public async Task<bool> AddPersonToFamilyAsync(Guid familyId, Guid personId)
        {
            var family = await _familyRepository.GetByIdAsync(familyId);
            var person = await _personRepository.GetByIdAsync(personId);
            if (person == null || person.IsDeleted || family == null || family.IsDeleted)
            {
                return false;
            }

            if (family.FamilyPersons != null)
            {
                var existsPersonInFamily = family.FamilyPersons.Any(x => x.PersonId == personId);
                if (existsPersonInFamily)
                {
                    return false;
                }
            }
            else
            {
                family.FamilyPersons = new List<FamilyPerson>();
            }

            family.FamilyPersons.Add(new FamilyPerson { FamilyId = familyId, PersonId = personId });
            family.LastUpdated = DateTime.UtcNow;

            _familyRepository.Update(family);
            await _familyRepository.SaveChangesAsync();

            return true;
        }

        /// <inheritdoc cref="IPersonFamilyConnectService.RemovePersonFromFamilyAsync(Guid, Guid)"/>
        public async Task<bool> RemovePersonFromFamilyAsync(Guid familyId, Guid personId)
        {
            var family = await _familyRepository.GetByIdAsync(familyId);
            var person = await _personRepository.GetByIdAsync(personId);
            if (person == null || person.IsDeleted || family == null || family.IsDeleted)
            {
                return false;
            }

            if (family.FamilyPersons == null)
            {
                return false;
            }

            var existsPersonInFamily = family.FamilyPersons.Any(x => x.PersonId == personId);
            if (!existsPersonInFamily)
            {
                return false;
            }

            family.FamilyPersons = family.FamilyPersons
                .Where(w => w.PersonId != personId)
                .ToList();
            family.LastUpdated = DateTime.UtcNow;

            _familyRepository.Update(family);
            await _familyRepository.SaveChangesAsync();

            return true;
        }
    }
}
