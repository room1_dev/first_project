﻿using AutoMapper;
using Domain.Entities;
using Services.Abstractions;
using Services.Contracts.Persons;
using Services.Repositories.Abstractions;

namespace Services.Implementations
{
    /// <summary>
    /// <inheritdoc cref="IPersonService"/>
    /// </summary>
    public class PersonService : IPersonService
    {
        private readonly IPersonRepository _personRepository;
        private readonly IFamilyRepository _familyRepository;
        private readonly IMapper _mapper;

        public PersonService(IPersonRepository personRepository, IFamilyRepository familyRepository, IMapper mapper)
        {
            _personRepository = personRepository;
            _familyRepository = familyRepository;
            _mapper = mapper;
        }

        /// <inheritdoc cref="IPersonService.Create(CreateUpdatePersonDTO)"/>
        public async Task<Guid?> Create(CreateUpdatePersonDTO creatingPersonDto)
        {
            var personToCreate = _mapper.Map<CreateUpdatePersonDTO, Person>(creatingPersonDto);
            var nowDateTime = DateTime.UtcNow;
            personToCreate.Created = nowDateTime;
            personToCreate.LastUpdated = nowDateTime;
            personToCreate.IsDeleted = false;
            var createdPerson = await _personRepository.AddAsync(personToCreate);
            await _personRepository.SaveChangesAsync();
            return createdPerson?.Id;
        }

        /// <inheritdoc cref="IPersonService.Delete(int)"/>
        public async Task<bool> Delete(Guid id)
        {
            var person = await _personRepository.GetByIdAsync(id);
            if (person == null || person.IsDeleted)
            {
                return false;
            }

            person.IsDeleted = true;
            person.LastUpdated = DateTime.UtcNow;

            await _personRepository.SaveChangesAsync();

            return true;
        }

        /// <inheritdoc cref="IPersonService.GetByIdAsync(Guid)"/>
        public async Task<PersonDTO?> GetByIdAsync(Guid id)
        {
            var person = await _personRepository.GetByIdAsync(id);
            if (person == null || person.IsDeleted)
            {
                return null;
            }

            var personDTO = _mapper.Map<Person, PersonDTO>(person);
            if (person.FamilyPersons != null && person.FamilyPersons.Any())
            {
                var familyList = new List<FamilyForPersonDTO>();
                foreach (var familyPersonsID in person.FamilyPersons)
                {
                    var family = await _familyRepository.GetByIdAsync(familyPersonsID.FamilyId);
                    if (family != null)
                    {
                        familyList.Add(new FamilyForPersonDTO { Id = family.Id, Name =  family.Name });
                    }
                }
                personDTO.Families = familyList;
            }

            return personDTO;
        }

        /// <inheritdoc cref="IPersonService.GetByIdAsync(Guid)"/>
        public async Task<List<PersonDTO>> GetByPhotoMainIdAsync(Guid id)
        {
            var personList = await _personRepository.GetByPhotoIdAsync(id);
            if (!personList.Any())
            {
                return new List<PersonDTO>();
            }
            return _mapper.Map<List<Person>, List<PersonDTO>>(personList);
        }

        /// <inheritdoc cref="IPersonService.Update(Guid, CreateUpdatePersonDTO)"/>
        public async Task<bool> Update(Guid id, CreateUpdatePersonDTO creatingPersonDto)
        {
            var person = await _personRepository.GetByIdAsync(id);
            if (person == null || person.IsDeleted)
            {
                return false;
            }

            person.FirstName = creatingPersonDto.FirstName;
            person.MiddleName = creatingPersonDto.MiddleName;
            person.LastName = creatingPersonDto.LastName;
            person.DateOfBirth = creatingPersonDto.DateOfBirth;
            person.DateOfDeath = creatingPersonDto.DateOfDeath;
            //person.Description = creatingPersonDto.Description;
            person.ImageId = creatingPersonDto?.ImageId;
            person.LastUpdated = DateTime.UtcNow;
            _personRepository.Update(person);
            await _personRepository.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// <inheritdoc cref="IPersonService.DeleteMainPhoto(Guid)"/>
        /// </summary>
        public async Task<bool> DeleteMainPhoto(Guid photoGuid)
        {
            var personList = await _personRepository.GetByPhotoIdAsync(photoGuid);
            if (!(personList?.Any() ?? false))
            {
                return false;
            }

            foreach (var person in personList)
            {
                person.ImageId = null;
                person.LastUpdated = DateTime.UtcNow;
                _personRepository.Update(person);
            }

            await _personRepository.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// <inheritdoc cref="IPersonService.GetFioByGuidArray(List{string})"/>
        /// </summary>
        /// <param name="guids"></param>
        /// <returns></returns>
        public async Task<Dictionary<Guid, string>> GetFioByGuidArray(List<Guid> guids)
        {
            if (!guids.Any())
            {
                return new Dictionary<Guid, string>();
            }

            var personList = await _personRepository.GetByIdArrayAsync(guids.ToArray());
            if (personList == null || !personList.Any())
            {
                return new Dictionary<Guid, string>();
            }
            
            return personList.Where(x => !x.IsDeleted).ToDictionary(x => x.Id, x => $"{x.FirstName} {x.MiddleName} {x.LastName}");
        }
    }
}