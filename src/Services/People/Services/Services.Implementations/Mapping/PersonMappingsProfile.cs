﻿using AutoMapper;
using Domain.Entities;
using Services.Contracts.Persons;

namespace Services.Implementations.Mapping
{
    public class PersonMappingsProfile : Profile
    {
        public PersonMappingsProfile()
        {
            CreateMap<CreateUpdatePersonDTO, Person>()
                .ForMember(m => m.Id, map => map.Ignore())
                .ForMember(m => m.IsDeleted, map => map.Ignore())
                .ForMember(m => m.Created, map => map.Ignore())
                .ForMember(m => m.LastUpdated, map => map.Ignore())
                .ForMember(m => m.FamilyPersons, map => map.Ignore())
                .ForMember(m => m.SourceRelashoinships, map => map.Ignore())
                .ForMember(m => m.DestRelashoinships, map => map.Ignore())
                .ForMember(m => m.UserID, map => map.Ignore())
                .ForMember(m => m.FirstName, map => map.MapFrom(m => m.FirstName))
                .ForMember(m => m.MiddleName, map => map.MapFrom(m => m.MiddleName))
                .ForMember(m => m.LastName, map => map.MapFrom(m => m.LastName))
                .ForMember(m => m.DateOfBirth, map => map.MapFrom(m => m.DateOfBirth))
                .ForMember(m => m.DateOfDeath, map => map.MapFrom(m => m.DateOfDeath))
                .ForMember(m => m.Gender, map => map.MapFrom(m => m.Gender))
                .ForMember(m => m.Description, map => map.MapFrom(m => m.Description));

            CreateMap<Person, PersonDTO>()
                .ForMember(m => m.Families, map => map.Ignore())
                .ForMember(m => m.UserID, map => map.Ignore())
                .ForMember(m => m.FirstName, map => map.MapFrom(m => m.FirstName))
                .ForMember(m => m.MiddleName, map => map.MapFrom(m => m.MiddleName))
                .ForMember(m => m.LastName, map => map.MapFrom(m => m.LastName))
                .ForMember(m => m.DateOfBirth, map => map.MapFrom(m => m.DateOfBirth))
                .ForMember(m => m.DateOfDeath, map => map.MapFrom(m => m.DateOfDeath))
                .ForMember(m => m.Gender, map => map.MapFrom(m => m.Gender))
                .ForMember(m => m.Description, map => map.MapFrom(m => m.Description));

        }
    }
}
