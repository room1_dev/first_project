﻿using AutoMapper;
using Domain.Entities;
using Services.Contracts.Families;

namespace Services.Implementations.Mapping
{
    public class FamilyMappingsProfile : Profile
    {
        public FamilyMappingsProfile()
        {
            CreateMap<CreateUpdateFamilyDTO, Family>()
                .ForMember(m => m.Id, map => map.Ignore())
                .ForMember(m => m.FamilyUsers, map => map.Ignore())
                .ForMember(m => m.IsDeleted, map => map.Ignore())
                .ForMember(m => m.Created, map => map.Ignore())
                .ForMember(m => m.LastUpdated, map => map.Ignore())
                .ForMember(m => m.FamilyPersons, map => map.Ignore())
                .ForMember(m => m.Name, map => map.MapFrom(m => m.Name))
                .ForMember(m => m.Description, map => map.MapFrom(m => m.Description));

            CreateMap<Family, FamilyDTO>()
                .ForMember(m => m.PersonList, map => map.Ignore())
                .ForMember(m => m.UserList, map => map.Ignore())
                .ForMember(m => m.Name, map => map.MapFrom(m => m.Name))
                .ForMember(m => m.Description, map => map.MapFrom(m => m.Description));
        }
    }
}
