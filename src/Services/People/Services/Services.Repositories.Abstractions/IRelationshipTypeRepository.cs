﻿using Domain.Entities;

namespace Services.Repositories.Abstractions
{
    public interface IRelationshipTypeRepository : IRepository<RelationshipType>
    {
        Task<RelationshipType> GetByTypeName(string typeName);
    }
}
