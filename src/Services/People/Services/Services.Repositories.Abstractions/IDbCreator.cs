﻿namespace Services.Repositories.Abstractions
{
    public interface IDbCreator
    {
        void Create();
    }
}
