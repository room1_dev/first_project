﻿using Domain.Entities;

namespace Services.Repositories.Abstractions
{
    public interface IPersonRepository : IRepository<Person>
    {
        Task<List<Person>> GetByPhotoIdAsync(Guid id);
    }
}
