﻿using Domain.Entities;

namespace Services.Repositories.Abstractions
{
    public interface IRepository<T>
        where T : BaseEntity
    {
        Task<T?> GetByIdAsync(Guid id);

        Task<List<T>> GetByIdArrayAsync(Guid[] ids);

        Task<List<T>> GetAllAsync();

        Task<T> AddAsync(T entity);

        void Update(T entity);

        Task SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}