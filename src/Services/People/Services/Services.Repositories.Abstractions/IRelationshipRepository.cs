﻿using Domain.Entities;

namespace Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозитория для работы с сущностью связей
    /// </summary>
    public interface IRelationshipRepository : IRepository<Relationship>
    {
        /// <summary>
        /// Получение списка связей персоны
        /// </summary>
        /// <param name="personGuid"></param>
        /// <param name="withIncludedPersonDest"></param>
        /// <returns></returns>
        Task<IList<Relationship>> GetByPersonGuidAsync(Guid personGuid, bool withIncludedPersonDest = true);

        /// <summary>
        /// Получить связь
        /// </summary>
        /// <param name="personID"></param>
        /// <param name="persondestID"></param>
        /// <param name="relationTypeID"></param>
        /// <returns></returns>
        Task<Relationship?> GetRelationshipAsync(Guid personID, Guid persondestID, Guid relationTypeID);
    }
}
