﻿using Domain.Entities;

namespace Services.Repositories.Abstractions
{
    public interface IFamilyRepository : IRepository<Family>
    {
        /// <summary>
        /// Получить список семей по юзеру
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<List<Family>> GetByUserId(Guid userId);
    }
}
