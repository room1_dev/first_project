﻿using Services.Contracts.Families;

namespace Services.Abstractions
{
    public interface IFamilyService
    {
        /// <summary>
        /// Получить семью
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        /// <returns> ДТО персоны. </returns>
        Task<FamilyDTO?> GetByIdAsync(Guid id);

        /// <summary>
        /// Получить семьи по юзеру
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        /// <returns> Список ДТО персон </returns>
        Task<List<FamilyDTO>> GetByUserIdAsync(Guid userId);

        /// <summary>
        /// Получить все семьи
        /// </summary>
        /// <returns> список ДТО семей </returns>
        Task<List<FamilyDTO>> GetAllAsync();

        /// <summary>
        /// Создать семью
        /// </summary>
        /// <param name="createUpdateFamilyDTO"> ДТО семьи </param>
        /// <returns> Идентификатор </returns>
        Task<Guid?> Create(CreateUpdateFamilyDTO createUpdateFamilyDTO);

        /// <summary>
        /// Изменить семью
        /// </summary>
        /// <param name="id"> Идентификатор </param>
        /// <param name="createUpdateFamilyDTO"> ДТО семьи </param>
        Task<bool> Update(Guid id, CreateUpdateFamilyDTO createUpdateFamilyDTO);

        /// <summary>
        /// Удалить семью
        /// </summary>
        /// <param name="id"> Идентификатор </param>
        Task<bool> Delete(Guid id);
    }
}
