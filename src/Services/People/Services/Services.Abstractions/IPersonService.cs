﻿using Services.Contracts.Persons;

namespace Services.Abstractions
{
    /// <summary>
    /// Сервис для работы с персонами
    /// </summary>
    public interface IPersonService
    {
        /// <summary>
        /// Получить персону
        /// </summary>
        /// <param name="id"> Идентификатор </param>
        /// <returns> ДТО персоны </returns>
        Task<PersonDTO?> GetByIdAsync(Guid id);

        /// <summary>
        /// Получить ФИО персоны по ИН картинки
        /// </summary>
        /// <param name="id">ИН картинки</param>
        /// <returns></returns>
        Task<List<PersonDTO>> GetByPhotoMainIdAsync(Guid id);

        /// <summary>
        /// Создать персону
        /// </summary>
        /// <param name="creatingLessonDto"> ДТО персоны </param>
        /// <returns> Идентификатор </returns>
        Task<Guid?> Create(CreateUpdatePersonDTO creatingPersonDto);

        /// <summary>
        /// Изменить персону
        /// </summary>
        /// <param name="id"> Идентификатор </param>
        /// <param name="updatingLessonDto"> ДТО персоны. </param>
        Task<bool> Update(Guid id, CreateUpdatePersonDTO creatingPersonDto);

        /// <summary>
        /// Удалить основное фото для персоны
        /// </summary>
        /// <param name="id"> Идентификатор фото </param>
        Task<bool> DeleteMainPhoto(Guid photoGuid);

        /// <summary>
        /// Удалить персону
        /// </summary>
        /// <param name="id"> Идентификатор </param>
        Task<bool> Delete(Guid id);

        /// <summary>
        /// Получить ФИО по списку пользователей.
        /// </summary>
        /// <returns>Словарь Id-Fio</returns>
        Task<Dictionary<Guid, string>> GetFioByGuidArray(List<Guid> guids);
    }
}