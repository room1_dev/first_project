﻿namespace Services.Abstractions
{
    public interface IPersonFamilyConnectService
    {
        /// <summary>
        /// Добавить персону в семью
        /// </summary>
        /// <param name="familyId">ID семьи</param>
        /// <param name="personId">ID персоны</param>
        /// <returns></returns>
        Task<bool> AddPersonToFamilyAsync(Guid familyId, Guid personId);

        /// <summary>
        /// Удалить персону из семьи
        /// </summary>
        /// <param name="familyId">ID семьи</param>
        /// <param name="personId">ID персоны</param>
        /// <returns></returns>
        Task<bool> RemovePersonFromFamilyAsync(Guid familyId, Guid personId);
    }
}
