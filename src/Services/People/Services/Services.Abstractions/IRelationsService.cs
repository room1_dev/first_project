﻿using Services.Contracts.Persons;
using Services.Contracts.Relations;

namespace Services.Abstractions
{
    /// <summary>
    /// Сервис для работы с родственными связями
    /// </summary>
    public interface IRelationsService
    {
        /// <summary>
        /// Получить всех прямых родственников человека
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        Task<PersonWithRelationsDTO?> GetPersonWithStrongRelations(Guid guid);

        /// <summary>
        /// Получить список родственников для семьи, форматировано для генеалогического древа
        /// </summary>
        /// <param name="guid">guid семьи</param>
        /// <returns></returns>
        Task<List<RelationForTreeDTO>> GetFamilyRelationsForGenTree(Guid guid);

        /// <summary>
        /// Получить список родственников для персоны, форматировано для генеалогического древа
        /// </summary>
        /// <param name="personID"></param>
        /// <returns></returns>
        Task<RelationForTreeDTO?> GetPersonRelationsForGenTree(Guid personID);

        /// <summary>
        /// Создать персону
        /// </summary>
        /// <param name="creatingLessonDto"> ДТО персоны </param>
        /// <returns> Идентификатор </returns>
        Task<Guid?> Create(CreateRelationDTO createRelationDto);
    }
}
