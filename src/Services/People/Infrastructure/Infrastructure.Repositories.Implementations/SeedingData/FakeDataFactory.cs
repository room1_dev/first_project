﻿using Domain.Entities;
using System.Runtime.InteropServices;

namespace Infrastructure.Repositories.Implementations
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Family> Families => new List<Family>()
        {
            new Family()
            {
                Id = Guid.Parse("894645DD-76B2-4DB5-8292-955445AF50B4"),
                Name = "Романовы",
                Description = "Царская семья",
                FamilyUsers = new List<FamilyUser>()
                {
                    new FamilyUser() {
                        FamilyId = Guid.Parse("894645DD-76B2-4DB5-8292-955445AF50B4"),
                        UserId = Guid.Parse("ECBC522F-3257-47C3-B468-5E454499E991")
                    }
                },
                IsDeleted = false
            }
        };

        public static IEnumerable<Person> Persons => new List<Person>()
        {
            new Person()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                FirstName = "Николай II",
                MiddleName = "Александрович",
                LastName = "Романов",
                DateOfBirth = DateTime.Parse("1868.05.18").ToUniversalTime(),
                DateOfDeath = DateTime.Parse("1918.07.17").ToUniversalTime(),
                Gender = (int)Gender.Male,
                FamilyPersons = new List<FamilyPerson>()
                {
                    new FamilyPerson()
                    {
                        FamilyId = Guid.Parse("894645DD-76B2-4DB5-8292-955445AF50B4"),
                        PersonId = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f")
                    }
                }
            },
            new Person()
            {
                Id = Guid.Parse("F35127BD-5CA2-46A1-9F9A-53AAF0EFA6AF"),
                FirstName = "Александра",
                MiddleName = "Фёдоровна",
                LastName = "Романова",
                DateOfBirth = DateTime.Parse("1872.06.06").ToUniversalTime(),
                DateOfDeath = DateTime.Parse("1918.07.17").ToUniversalTime(),
                Gender = (int)Gender.Female,
                FamilyPersons = new List<FamilyPerson>()
                {
                    new FamilyPerson()
                    {
                        FamilyId = Guid.Parse("894645DD-76B2-4DB5-8292-955445AF50B4"),
                        PersonId = Guid.Parse("F35127BD-5CA2-46A1-9F9A-53AAF0EFA6AF")
                    }
                }
            },
            new Person()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                FirstName = "Ольга",
                MiddleName = "Николаевна",
                LastName = "Романова",
                DateOfBirth = DateTime.Parse("1895.11.15").ToUniversalTime(),
                DateOfDeath = DateTime.Parse("1918.07.17").ToUniversalTime(),
                Gender = (int)Gender.Female,
                FamilyPersons = new List<FamilyPerson>()
                {
                    new FamilyPerson()
                    {
                        FamilyId = Guid.Parse("894645DD-76B2-4DB5-8292-955445AF50B4"),
                        PersonId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895")
                    }
                }
            },
            new Person()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435896"),
                FirstName = "Татьяна",
                MiddleName = "Николаевна",
                LastName = "Романова",
                DateOfBirth = DateTime.Parse("1897.06.10").ToUniversalTime(),
                DateOfDeath = DateTime.Parse("1918.07.17").ToUniversalTime(),
                Gender = (int)Gender.Female,
                FamilyPersons = new List<FamilyPerson>()
                {
                    new FamilyPerson()
                    {
                        FamilyId = Guid.Parse("894645DD-76B2-4DB5-8292-955445AF50B4"),
                        PersonId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435896")
                    }
                }
            },
            new Person()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435897"),
                FirstName = "Мария",
                MiddleName = "Николаевна",
                LastName = "Романова",
                DateOfBirth = DateTime.Parse("1899.06.26").ToUniversalTime(),
                DateOfDeath = DateTime.Parse("1918.07.17").ToUniversalTime(),
                Gender = (int)Gender.Female,
                FamilyPersons = new List<FamilyPerson>()
                {
                    new FamilyPerson()
                    {
                        FamilyId = Guid.Parse("894645DD-76B2-4DB5-8292-955445AF50B4"),
                        PersonId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435897")
                    }
                }
            },
            new Person()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435898"),
                FirstName = "Анастасия",
                MiddleName = "Николаевна",
                LastName = "Романова",
                DateOfBirth = DateTime.Parse("1901.06.18").ToUniversalTime(),
                DateOfDeath = DateTime.Parse("1918.07.17").ToUniversalTime(),
                Gender = (int)Gender.Female,
                FamilyPersons = new List<FamilyPerson>()
                {
                    new FamilyPerson()
                    {
                        FamilyId = Guid.Parse("894645DD-76B2-4DB5-8292-955445AF50B4"),
                        PersonId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435898")
                    }
                }
            },
            new Person()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435899"),
                FirstName = "Алексей",
                MiddleName = "Николаевич",
                LastName = "Романов",
                DateOfBirth = DateTime.Parse("1904.08.12").ToUniversalTime(),
                DateOfDeath = DateTime.Parse("1918.07.17").ToUniversalTime(),
                Gender = (int)Gender.Male,
                FamilyPersons = new List<FamilyPerson>()
                {
                    new FamilyPerson()
                    {
                        FamilyId = Guid.Parse("894645DD-76B2-4DB5-8292-955445AF50B4"),
                        PersonId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435899")
                    }
                }
            },
        };

        public static IEnumerable<Relationship> Relationships => new List<Relationship>()
        {
            // супруги
            new Relationship()
            {
                PersonSourceId = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                PersonDestId = Guid.Parse("F35127BD-5CA2-46A1-9F9A-53AAF0EFA6AF"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC7")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("F35127BD-5CA2-46A1-9F9A-53AAF0EFA6AF"),
                PersonDestId = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC7")
            },
            // Николай II - дети
            new Relationship()
            {
                PersonSourceId = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC6")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                PersonDestId = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC5")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435896"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC6")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435896"),
                PersonDestId = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC5")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435897"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC6")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435897"),
                PersonDestId = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC5")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435898"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC6")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435898"),
                PersonDestId = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC5")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435899"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC6")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435899"),
                PersonDestId = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC5")
            },
            // Александра Федоровна - дети
            new Relationship()
            {
                PersonSourceId = Guid.Parse("F35127BD-5CA2-46A1-9F9A-53AAF0EFA6AF"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC6")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                PersonDestId = Guid.Parse("F35127BD-5CA2-46A1-9F9A-53AAF0EFA6AF"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC5")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("F35127BD-5CA2-46A1-9F9A-53AAF0EFA6AF"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435896"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC6")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435896"),
                PersonDestId = Guid.Parse("F35127BD-5CA2-46A1-9F9A-53AAF0EFA6AF"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC5")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("F35127BD-5CA2-46A1-9F9A-53AAF0EFA6AF"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435897"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC6")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435897"),
                PersonDestId = Guid.Parse("F35127BD-5CA2-46A1-9F9A-53AAF0EFA6AF"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC5")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("F35127BD-5CA2-46A1-9F9A-53AAF0EFA6AF"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435898"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC6")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435898"),
                PersonDestId = Guid.Parse("F35127BD-5CA2-46A1-9F9A-53AAF0EFA6AF"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC5")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("F35127BD-5CA2-46A1-9F9A-53AAF0EFA6AF"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435899"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC6")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435899"),
                PersonDestId = Guid.Parse("F35127BD-5CA2-46A1-9F9A-53AAF0EFA6AF"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC5")
            },
            // сестры - братья
            //1
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435896"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC8")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435896"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC8")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435897"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC8")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435897"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC8")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435898"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC8")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435898"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC8")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435899"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC8")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435899"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC8")
            },
            //2
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435896"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435897"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC8")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435897"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435896"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC8")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435896"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435898"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC8")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435898"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435896"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC8")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435896"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435899"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC8")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435899"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435896"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC8")
            },
            //3
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435897"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435898"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC8")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435898"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435897"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC8")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435897"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435899"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC8")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435899"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435897"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC8")
            },
            //4
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435898"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435899"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC8")
            },
            new Relationship()
            {
                PersonSourceId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435899"),
                PersonDestId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435898"),
                RelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC8")
            },
        };
    }

   
}
