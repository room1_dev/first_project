﻿using Domain.Entities;
using Infrastructure.EntityFramework;
using Infrastructure.Repositories.Implementations.SeedingData;
using Microsoft.EntityFrameworkCore;
using Services.Repositories.Abstractions;

namespace Infrastructure.Repositories.Implementations
{
    public class DbCreator : IDbCreator
    {
        private readonly DatabaseContext _dataContext;

        public DbCreator(DatabaseContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void Create()
        {
            var familyFirst = _dataContext.Set<Family>().FirstOrDefault();
                
            if (familyFirst == null)
            {
                _dataContext.AddRange(DataRelationshipTypesFactory.RelationshipTypes);
                _dataContext.SaveChanges();

                _dataContext.AddRange(FakeDataFactory.Families);
                _dataContext.SaveChanges();

                _dataContext.AddRange(FakeDataFactory.Persons);
                _dataContext.SaveChanges();

                _dataContext.AddRange(FakeDataFactory.Relationships);
                _dataContext.SaveChanges();
            }
        }
    }
}
