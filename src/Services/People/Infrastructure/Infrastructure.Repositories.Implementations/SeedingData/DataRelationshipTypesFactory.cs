﻿using Domain.Entities;

namespace Infrastructure.Repositories.Implementations.SeedingData
{
    /// <summary>
    /// Создание нужных связей
    /// </summary>
    public static class DataRelationshipTypesFactory
    {
        public static IEnumerable<RelationshipType> RelationshipTypes => new List<RelationshipType>()
        {
            new RelationshipType()
            {
                Id = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC5"),
                Type = "Parent",
                ReverseRelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC6")
            },
            new RelationshipType()
            {
                Id = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC6"),
                Type = "Child",
                ReverseRelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC5")
            },
            new RelationshipType()
            {
                Id = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC7"),
                Type = "Spouse",
                ReverseRelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC7")
            },
            new RelationshipType()
            {
                Id = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC8"),
                Type = "Sibling",
                ReverseRelationshipTypeId = Guid.Parse("00DA446B-1DE5-44F9-A18E-6EFBAFC13AC8")
            }
        };
    }
}
