﻿using Domain.Entities;
using Infrastructure.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Services.Repositories.Abstractions;

namespace Infrastructure.Repositories.Implementations
{
    public class RelationshipRepository : Repository<Relationship>, IRelationshipRepository
    {
        public RelationshipRepository(DatabaseContext dataContext) : base(dataContext)
        {

        }

        public async Task<IList<Relationship>> GetByPersonGuidAsync(Guid personGuid, bool withIncludedPersonDest = true)
        {
            var query = _dataContext.Set<Relationship>().AsQueryable();

            if (withIncludedPersonDest)
            {
                query = query
                .Include(c => c.PersonDest);
            }

            query = query
                .Include(c => c.RelationshipType)
                .Where(c => c.PersonSourceId == personGuid)
                .Where(c => !c.PersonDest.IsDeleted);

            return await query.ToListAsync();
        }

        public async Task<Relationship?> GetRelationshipAsync(Guid personID, Guid persondestID, Guid relationTypeID)
        {
            var query = _dataContext.Set<Relationship>().AsQueryable()
                .Where(r => r.PersonSourceId == personID && r.PersonDestId == persondestID && r.RelationshipTypeId == relationTypeID);

            return await query.FirstOrDefaultAsync();
        }
    }
}
