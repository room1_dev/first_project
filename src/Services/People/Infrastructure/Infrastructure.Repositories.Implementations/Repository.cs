﻿using Domain.Entities;
using Services.Repositories.Abstractions;
using Infrastructure.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories.Implementations
{
    public class Repository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected readonly DatabaseContext _dataContext;

        public Repository(DatabaseContext dataContext)
        {
            _dataContext = dataContext;
        }

        public virtual async Task<T?> GetByIdAsync(Guid id)
        {
            return await _dataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public virtual async Task<List<T>> GetByIdArrayAsync(Guid[] ids)
        {
            return await _dataContext.Set<T>().Where(x => ids.Contains(x.Id)).ToListAsync();
        }

        public virtual async Task<List<T>> GetAllAsync()
        {
            return await _dataContext.Set<T>().ToListAsync();
        }

        public virtual async Task<T> AddAsync(T entity)
        {
            return (await _dataContext.Set<T>().AddAsync(entity)).Entity;
        }

        public virtual void Update(T entity)
        {
            _dataContext.Entry(entity).State = EntityState.Modified;
        }

        /// <summary>
        /// Сохранить изменения.
        /// </summary>
        public virtual async Task SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            await _dataContext.SaveChangesAsync(cancellationToken);
        }
    }
}
