﻿using Domain.Entities;
using Infrastructure.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Services.Repositories.Abstractions;

namespace Infrastructure.Repositories.Implementations
{
    public class RelationshipTypeRepository : Repository<RelationshipType>, IRelationshipTypeRepository
    {
        public RelationshipTypeRepository(DatabaseContext dataContext) : base(dataContext)
        {
        }

        public async Task<RelationshipType> GetByTypeName(string typeName)
        {
            var query = _dataContext.Set<RelationshipType>().AsQueryable();

            query = query
                .Where(c => c.Type == typeName);

            return await query.FirstOrDefaultAsync();
        }
    }
}
