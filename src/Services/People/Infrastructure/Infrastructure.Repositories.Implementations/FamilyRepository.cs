﻿using Domain.Entities;
using Infrastructure.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Services.Repositories.Abstractions;

namespace Infrastructure.Repositories.Implementations
{
    public class FamilyRepository : Repository<Family>, IFamilyRepository
    {
        public FamilyRepository(DatabaseContext dataContext) : base(dataContext)
        {

        }

        public override async Task<Family?> GetByIdAsync(Guid id)
        {
            var query = _dataContext.Set<Family>().AsQueryable();

            query = query
                .Include(c => c.FamilyPersons)
                .Include(c => c.FamilyUsers)
                .Where(c => c.Id == id)
                .Where(c => c.IsDeleted == false);

            return await query.SingleOrDefaultAsync();
        }

        public override async Task<List<Family>> GetAllAsync()
        {
            var query = _dataContext.Set<Family>().AsQueryable();

            query = query
                .Where(c => c.IsDeleted == false);

            return await query.ToListAsync();
        }

        public async Task<List<Family>> GetByUserId(Guid userId)
        {
            var familyGuidList = _dataContext.Set<FamilyUser>().AsQueryable()
                .Where(p => p.UserId == userId)
                .Select(p => p.FamilyId)
                .Distinct()
                .ToList();

            var query = _dataContext.Set<Family>().AsQueryable()
                .Where(c => familyGuidList.Contains(c.Id))
                .Where(c => c.IsDeleted == false);

            return await query.ToListAsync();
        }
    }
}
