﻿using Domain.Entities;
using Infrastructure.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Services.Repositories.Abstractions;

namespace Infrastructure.Repositories.Implementations
{
    public class PersonRepository : Repository<Person>, IPersonRepository
    {
        public PersonRepository(DatabaseContext dataContext) : base(dataContext)
        {

        }

        public override async Task<Person> GetByIdAsync(Guid id)
        {
            var query = _dataContext.Set<Person>().AsQueryable();

            query = query
                .Include(c => c.FamilyPersons)
                .Where(c => c.Id == id);

            return await query.SingleOrDefaultAsync();
        }

        public async Task<List<Person>> GetByPhotoIdAsync(Guid id)
        {
            var query = _dataContext.Set<Person>().AsQueryable();

            query = query
                .Where(c => c.ImageId == id);

            return await query.ToListAsync();
        }
    }
}