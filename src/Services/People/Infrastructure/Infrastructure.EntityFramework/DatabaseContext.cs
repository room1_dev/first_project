﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Infrastructure.EntityFramework
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options) 
        {
            Database.Migrate();
        }

        public DbSet<Family>? Families { get; set; }
        public DbSet<Person>? Persons { get; set; }
        public DbSet<RelationshipType>? RelationshipTypes { get; set; }
        public DbSet<Relationship>? Relationships { get; set; }
        public DbSet<FamilyPerson>? FamilyPersons { get; set; }
        public DbSet<FamilyUser>? FamilyUsers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FamilyPerson>()
                .HasOne(bc => bc.Family)
                .WithMany(w => w.FamilyPersons)
                .HasForeignKey(s => s.FamilyId);

            modelBuilder.Entity<FamilyPerson>()
                .HasOne(bc => bc.Person)
                .WithMany(w => w.FamilyPersons)
                .HasForeignKey(s => s.PersonId);

            modelBuilder.Entity<FamilyPerson>().HasKey(sc => new { sc.FamilyId, sc.PersonId });

            modelBuilder.Entity<Relationship>()
                .HasOne(bc => bc.PersonSource)
                .WithMany(r => r.SourceRelashoinships)
                .HasForeignKey(f => f.PersonSourceId);

            modelBuilder.Entity<Relationship>()
                .HasOne(bc => bc.PersonDest)
                .WithMany(r => r.DestRelashoinships)
                .HasForeignKey(f => f.PersonDestId);

            modelBuilder.Entity<Relationship>()
               .HasOne(bc => bc.RelationshipType)
               .WithMany(r => r.Relashionships)
               .HasForeignKey(f => f.RelationshipTypeId);

            modelBuilder.Entity<FamilyUser>()
               .HasOne(bc => bc.Family)
               .WithMany(r => r.FamilyUsers)
               .HasForeignKey(f => f.FamilyId);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);
        }
    }
}
