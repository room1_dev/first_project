﻿namespace Domain.Entities
{
    /// <summary>
    /// Сущность, показывающая относится ли персона к семье
    /// </summary>
    public class FamilyPerson : BaseEntity
    {
        public Guid FamilyId { get; set; }
        public Family Family { get; set; }
        public Guid PersonId { get; set; }
        public Person Person { get; set; }
    }
}
