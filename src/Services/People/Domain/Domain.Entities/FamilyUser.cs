﻿namespace Domain.Entities
{
    public class FamilyUser : BaseEntity
    {
        public Guid FamilyId { get; set; }
        public Family Family { get; set; }
        public Guid UserId { get; set; }
    }
}
