﻿namespace Domain.Entities
{
    /// <summary>
    /// Сущность - тип семейной связи (Child, Parent, Sibling, Spouse)
    /// </summary>
    public class RelationshipType : BaseEntity
    {
        /// <summary>
        /// Наименование типа связи
        /// </summary>
        public string Type { get; set; } = string.Empty;

        /// <summary>
        /// Зеркальноая связь (Родитель - ребенок)
        /// </summary>
        public Guid ReverseRelationshipTypeId { get; set; }

        /// <summary>
        /// Список отношений для каждого типа
        /// </summary>
        public IList<Relationship> Relashionships { get; set; } = new List<Relationship>();
    }
}
