﻿namespace Domain.Entities
{
    /// <summary>
    /// Сущность семьи
    /// </summary>
    public class Family : BaseEntity
    {
        /// <summary>
        /// Название семьи
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Описание семьи
        /// </summary>
        public string Description { get; set; } = string.Empty;

        /// <summary>
        /// Члены семьи
        /// </summary>
        public IList<FamilyPerson> FamilyPersons { get; set; } = new List<FamilyPerson>();

        /// <summary>
        /// Связь семьи и юзера
        /// </summary>
        public IList<FamilyUser> FamilyUsers { get; set; } = new List<FamilyUser>();

        /// <summary>
        /// Служебная информация: дата создания
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// Служебная информация: обновлено
        /// </summary>
        public DateTime LastUpdated { get; set; }

        /// <summary>
        /// Служебная информация: удалено ли?
        /// </summary>
        public bool IsDeleted { get; set; } = false;
    }
}
