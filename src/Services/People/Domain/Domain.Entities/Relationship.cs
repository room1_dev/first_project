﻿namespace Domain.Entities
{
    /// <summary>
    /// Семейные связи
    /// </summary>
    public class Relationship : BaseEntity
    {
        /// <summary>
        /// Guid - Прямая связь (он яывляется кем мне)
        /// </summary>
        public Guid PersonSourceId { get; set; }

        /// <summary>
        /// Прямая связь (он яывляется кем мне)
        /// </summary>
        public Person PersonSource { get; set; }

        /// <summary>
        /// Guid - Обратная связь (я являюсь кем ему)
        /// </summary>
        public Guid PersonDestId { get; set; }
        
        /// <summary>
        /// Обратная связь (я являюсь кем ему)
        /// </summary>
        public Person PersonDest { get; set; }

        /// <summary>
        /// Guid - Тип семейной связи
        /// </summary>
        public Guid RelationshipTypeId { get; set; }

        /// <summary>
        /// Тип семейной связи
        /// </summary>
        public RelationshipType RelationshipType { get; set; }
    }
}
