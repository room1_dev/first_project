﻿namespace Domain.Entities
{
    /// <summary>
    /// Сущность персона
    /// </summary>
    public class Person : BaseEntity
    {
        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; } = string.Empty;

        /// <summary>
        /// Отчество
        /// </summary>
        public string MiddleName { get; set; } = string.Empty;

        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; } = string.Empty;

        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// Дата смерти, если человек жив = null
        /// </summary>
        public DateTime? DateOfDeath { get; set; }

        /// <summary>
        /// Пол
        /// </summary>
        public int Gender { get; set; }

        /// <summary>
        /// Описание для персоны
        /// </summary>
        public string Description { get; set; } = string.Empty;


        /// <summary>
        /// Связь юзера с персоной (это я)
        /// </summary>
        public Guid? UserID { get; set; }

        /// <summary>
        /// Связь с семьёй
        /// </summary>
        public IList<FamilyPerson> FamilyPersons { get; set; } = new List<FamilyPerson>();

        /// <summary>
        /// Дата добавления записи
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// Последний раз обновлено
        /// </summary>
        public DateTime LastUpdated { get; set; }

        /// <summary>
        /// Удалено
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Исходные семейные связи (я - он)
        /// </summary>
        public IList<Relationship> SourceRelashoinships { get; set; } = new List<Relationship>();

        /// <summary>
        /// Обратные семейные связи (он - я)
        /// </summary>
        public IList<Relationship> DestRelashoinships { get; set; } = new List<Relationship>();

        /// <summary>
        /// ID картинки для персоны
        /// </summary>
        public Guid? ImageId { get; set; } = null;
    }
}
