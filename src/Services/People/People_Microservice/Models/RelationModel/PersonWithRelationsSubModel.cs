﻿namespace People_Microservice.Models.RelationModel
{
    public class PersonWithRelationsSubModel
    {
        public Guid PersonGuid { get; set; }
        public string PersonName { get; set; } = string.Empty;

        public string RelationType { get; set; } = string.Empty;
    }
}
