﻿namespace People_Microservice.Models.RelationModel
{
    public class CreateRelationModel
    {
        public Guid PersonSourceGuid { get; set; }
        public Guid PersonDestGuid { get; set; }
        public string RelationTypeName { get; set; }
    }
}
