﻿namespace People_Microservice.Models.RelationModel
{
    public class PersonWithRelationsModel
    {
        public Guid PersonGuid { get; set; }
        public string PersonName { get; set; } = string.Empty;
        public IList<PersonWithRelationsSubModel> RelationPersons { get; set; } = new List<PersonWithRelationsSubModel>();
    }
}
