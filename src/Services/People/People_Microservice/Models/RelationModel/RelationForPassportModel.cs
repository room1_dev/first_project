﻿using SharedBus;

namespace People_Microservice.Models.RelationModel
{
    public class RelationForPassportModel
    {
        public Guid PersonGuid { get; set; }

        public string Name { get; set; }

        public int Gender { get; set; }

        public DateTime DateOfBirth { get; set; }

        public PersonWithRelationsSubModel[] Parents { get; set; }

        public PersonWithRelationsSubModel[] Siblings { get; set; }

        public PersonWithRelationsSubModel[] Spouses { get; set; }

        public PersonWithRelationsSubModel[] Children { get; set; }
        public string Image { get; set; } = "https://www.kindpng.com/picc/m/256-2560186_user-name-icon-hd-png-download.png";

        /// <summary>
        /// ID картинки для персоны
        /// </summary>
        public Guid? ImageId { get; set; } = null;
    }
}
