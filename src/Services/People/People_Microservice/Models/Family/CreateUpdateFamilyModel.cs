﻿namespace People_Microservice.Models.Family
{
    public class CreateUpdateFamilyModel
    {
        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Описание семьи
        /// </summary>
        public string Description { get; set; } = string.Empty;

        /// <summary>
        /// Юзер, который создал, сделал обновление
        /// </summary>
        public Guid UserID { get; set; }
    }
}
