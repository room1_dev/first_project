﻿using Services.Contracts.Families;

namespace People_Microservice.Models.Family
{
    public class FamilyModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Описание семьи
        /// </summary>
        public string Description { get; set; } = string.Empty;

        /// <summary>
        /// Список персон в семье
        /// </summary>
        public List<PersonForFamilyDTO> PersonList { get; set; } = new List<PersonForFamilyDTO>();

        /// <summary>
        /// Список юзеров для семьи
        /// </summary>
        public List<Guid> UserList { get; set; } = new List<Guid>();
    }
}
