﻿using AutoMapper;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using People_Microservice.ConnectionServices.Abstractions;
using People_Microservice.Models.RelationModel;
using Services.Abstractions;
using Services.Contracts.Relations;
using SharedBus;

namespace People_Microservice.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RelationshipController : Controller
    {
        private readonly IRelationsService _service;
        private readonly IMapper _mapper;
        private readonly IPublishEndpoint _publishEndpoint;
        private readonly IPicturePathRPC _grpcPicturePath;

        public RelationshipController(IRelationsService service, IMapper mapper, IPublishEndpoint publishEndpoint, IPicturePathRPC grpc)
        {
            _service = service;
            _mapper = mapper;
            _publishEndpoint = publishEndpoint;
            _grpcPicturePath = grpc;
        }

        [HttpGet("SimpleModel/{id}")]
        public async Task<IActionResult> GetForPerson(Guid id)
        {
            var personWithRelations = _mapper.Map<PersonWithRelationsModel>(await _service.GetPersonWithStrongRelations(id));

            if (personWithRelations == null)
                return NotFound();

            return Ok(personWithRelations);
        }

        [HttpGet("ForGenTreeModel/{familyGuid}")]
        public async Task<IActionResult> GetForGenTree(Guid familyGuid)
        {
            var personListWithRelationsDto = await _service.GetFamilyRelationsForGenTree(familyGuid);

            var personListWithRelations = personListWithRelationsDto.Select(f => _mapper.Map<RelationForTreeModel>(f)).ToList();

            if (!personListWithRelations.Any())
                return NotFound();

            var imageGuidAr = personListWithRelations
                .Where(w => w != null && w.ImageId.HasValue)
                .Select(s => s.ImageId.Value).Distinct().ToArray();
            if (imageGuidAr.Any())
            {
                var dict = await _grpcPicturePath.Get(imageGuidAr);
                foreach (var kvp in dict)
                {
                    var persons = personListWithRelations.Where(w => w.ImageId == kvp.Key).ToArray();
                    foreach (var person in persons)
                    {
                        person.Image = kvp.Value;
                    }
                }
            }

            await _publishEndpoint.Publish(new BusFamilySchema
            {
                Result = personListWithRelations.ToArray(),
                FamilyId = familyGuid
            });

            return Ok(null);
        }

        [HttpGet("PersonForGenTreeModel/{personGuid}")]
        public async Task<IActionResult> GetPersonForGenTree(Guid personGuid)
        {
            var personWithRelationsDto = await _service.GetPersonRelationsForGenTree(personGuid);
            if (personWithRelationsDto == null)
                return NotFound();

            var personModel = _mapper.Map<RelationForPassportModel>(personWithRelationsDto);

            return Ok(personModel);
        }

        [HttpPost]
        public async Task<IActionResult> Add(CreateRelationModel creatingRelationModel)
        {
            return Ok(await _service.Create(_mapper.Map<CreateRelationModel, CreateRelationDTO>(creatingRelationModel)));
        }

        [HttpGet("TestConnectionService")]
        public async Task<Dictionary<Guid, string>> TestConnectionService()
        {
            var dict = await _grpcPicturePath.Get(new Guid[] { Guid.Parse("a4f32140-7e63-436c-83a5-84a5e7822617") });
            return dict;
        }
    }
}
