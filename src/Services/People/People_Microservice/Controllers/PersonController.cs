﻿using AutoMapper;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using People_Microservice.Models.Person;
using Services.Abstractions;
using Services.Contracts.Persons;
using Services.Implementations;

namespace People_Microservice.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PersonController : Controller
    {
        private readonly IPersonService _service;
        private readonly IMapper _mapper;
        private readonly IPersonFamilyConnectService _personFamilyConnectService;

        public PersonController(IPersonService service, IMapper mapper, IPersonFamilyConnectService personFamilyConnectService)
        {
            _service = service;
            _mapper = mapper;
            _personFamilyConnectService = personFamilyConnectService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id, [FromHeader] Guid userId)
        {
            Console.WriteLine($"-------->>>>>>> user id from JWT = {userId}");

            var person = _mapper.Map<PersonModel>(await _service.GetByIdAsync(id));

            if (person == null)
                return NotFound();

            return Ok(person);
        }

        [HttpGet("GetFioByMainPhoto/{id}")]
        public async Task<IActionResult> GetFioByMainPhoto(Guid id, [FromHeader] Guid userId)
        {
            Console.WriteLine($"-------->>>>>>> user id from JWT = {userId}");

            var personList = _mapper.Map<List<PersonModel>>(await _service.GetByPhotoMainIdAsync(id));

            return Ok(personList);
        }

        [HttpPost]
        public async Task<IActionResult> Add(CreateUpdatePersonModel creatingPersonDto)
        {
            return Ok(await _service.Create(_mapper.Map<CreateUpdatePersonModel, CreateUpdatePersonDTO>(creatingPersonDto)));
        }

        [HttpPost("withFamily/{familyId}")]
        public async Task<IActionResult> AddPersonWithFamily(Guid familyId, CreateUpdatePersonModel creatingPersonDto)
        {
            var personGuid = await _service.Create(_mapper.Map<CreateUpdatePersonModel, CreateUpdatePersonDTO>(creatingPersonDto));
            if (!personGuid.HasValue)
            {
                return BadRequest();
            }
            var result = await _personFamilyConnectService.AddPersonToFamilyAsync(familyId, personGuid.Value);
            if (!result)
            {
                return BadRequest();
            }
            return Ok();
        }

        [HttpPut("updatePerson/{id}")]
        public async Task<IActionResult> Edit(Guid id, CreateUpdatePersonModel creatingPersonDto)
        {
            var result = await _service.Update(id, _mapper.Map<CreateUpdatePersonModel, CreateUpdatePersonDTO>(creatingPersonDto));
            if (!result)
            {
                return NotFound();
            }
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var result = await _service.Delete(id);
            if (!result)
            {
                return NotFound();
            }
            return Ok();
        }

        [HttpPut("deleteMainPhoto/{id}")]
        public async Task<IActionResult> EditPersonDeleteMainPhoto(Guid id)
        {
            var result = await _service.DeleteMainPhoto(id);
            if (!result)
            {
                return NotFound();
            }
            return Ok();
        }
    }
}
