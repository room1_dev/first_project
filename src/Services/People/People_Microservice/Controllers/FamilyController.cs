﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using People_Microservice.Models.Family;
using Services.Abstractions;
using Services.Contracts.Families;

namespace People_Microservice.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class FamilyController : Controller
    {
        private readonly IFamilyService _familyService;
        private readonly IPersonFamilyConnectService _personFamilyConnectService;
        private readonly IMapper _mapper;

        public FamilyController(IFamilyService service, IPersonFamilyConnectService personFamilyConnectService, IMapper mapper)
        {
            _familyService = service;
            _personFamilyConnectService = personFamilyConnectService;
            _mapper = mapper;
        }

        [HttpGet("withpersons/{familyid}")]
        public async Task<IActionResult> GetWithPersons(Guid familyid)
        {
            var family = _mapper.Map<FamilyModel>(await _familyService.GetByIdAsync(familyid));

            if (family == null)
                return NotFound();

            return Ok(family);
        }

        [HttpGet("foruser/{userId}")]
        public async Task<IActionResult> GetByUser(Guid userId)
        {
            var familyListDto = await _familyService.GetByUserIdAsync(userId);

            var familyListModel = familyListDto.Select(f => _mapper.Map<SimpleFamilyModel>(f)).ToList();

            if (familyListModel == null)
                return NotFound();

            return Ok(familyListModel);
        }

        [HttpGet("all")]
        public async Task<IActionResult> GetAllWithoutPersons()
        {
            var familyListDto = await _familyService.GetAllAsync();

            var familyListModel = familyListDto.Select(f => _mapper.Map<SimpleFamilyModel>(f)).ToList();

            if (familyListModel == null)
                return NotFound();

            return Ok(familyListModel);
        }

        [HttpPost]
        public async Task<IActionResult> Add(CreateUpdateFamilyModel creatingFamilyDto)
        {
            return Ok(await _familyService.Create(_mapper.Map<CreateUpdateFamilyModel, CreateUpdateFamilyDTO>(creatingFamilyDto)));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Edit(Guid id, CreateUpdateFamilyModel creatingFamilyDto)
        {
            var result = await _familyService.Update(id, _mapper.Map<CreateUpdateFamilyModel, CreateUpdateFamilyDTO>(creatingFamilyDto));
            if (!result)
            {
                return NotFound();
            }
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var result = await _familyService.Delete(id);
            if (!result)
            {
                return NotFound();
            }
            return Ok();
        }

        [HttpPut("addPerson/{familyId}")]
        public async Task<IActionResult> AddPersonToFamily(Guid familyId, Guid personId)
        {
            var result = await _personFamilyConnectService.AddPersonToFamilyAsync(familyId, personId);
            if (!result)
            {
                return BadRequest();
            }
            return Ok();
        }

        [HttpPut("removePersonFromFamily/{familyId}")]
        public async Task<IActionResult> RemovePersonFromFamily(Guid familyId, Guid personId)
        {
            var result = await _personFamilyConnectService.RemovePersonFromFamilyAsync(familyId, personId);
            if (!result)
            {
                return BadRequest();
            }
            return Ok();
        }
    }
}
