﻿namespace People_Microservice.ConnectionServices.Abstractions
{
    /// <summary>
    /// Сервис взаимодействия через Remote Procedure Call для получения пути картинки
    /// </summary>
    public interface IPicturePathRPC
    {
        /// <summary>
        /// Получить путь картинки
        /// </summary>
        /// <param name="guidPicAr">список Guid картинок</param>
        /// <returns>Словарь гуид картинки - путь</returns>
        Task<Dictionary<Guid, string>> Get(IEnumerable<Guid> guidPicAr);
    }
}
