﻿using Grpc.Core;
using Grpc.Net.Client;
using GrpcService;
using People_Microservice.ConnectionServices.Abstractions;
using People_Microservice.Settings;

namespace People_Microservice.ConnectionServices.Grpc.Services
{
    /// <summary>
    /// Реализация Remote Procedure Call в виде gRPC для получения пути картинки
    /// </summary>
    public class PicturePathgRPC : IPicturePathRPC
    {
        private readonly ApplicationSettings _settings;
        private readonly ILogger<PicturePathgRPC> _logger;

        public PicturePathgRPC(ApplicationSettings settings, ILogger<PicturePathgRPC> logger)
        {
            _settings = settings;
            _logger = logger;
        }

        public async Task<Dictionary<Guid, string>> Get(IEnumerable<Guid> guidPicAr)
        {
            using var channel = GrpcChannel.ForAddress(_settings.GrpcServerAdress);
            var client = new Photopath.PhotopathClient(channel);

            var pathRequest = new GetPathRequest();
            pathRequest.Guid.AddRange(guidPicAr.Select(s => s.ToString()));

            var dict = new Dictionary<Guid, string>();
            PathReply? reply;
            try
            {
                reply = await client.GetPathAsync(pathRequest);
            }
            catch (RpcException ex)
            {
                _logger.LogError("Status code: " + ex.Status.StatusCode);
                _logger.LogError("Message: " + ex.Status.Detail);
                return dict;
            }

            if (reply != null)
            {
                _logger.LogInformation("Got smth from gRPC:");
                foreach (var kv in reply.Answer)
                {
                    _logger.LogInformation("Key: " + kv.Key + " Value: " + kv.Value);
                    dict.Add(Guid.Parse(kv.Key), kv.Value);
                }
            }
            else
            {
                _logger.LogInformation("Nothing from gRPC");
            }

            return dict;

        }
    }
}
