﻿using Microsoft.AspNetCore.SignalR.Client;
using People_Microservice.ConnectionServices.Abstractions;
using People_Microservice.Settings;

namespace People_Microservice.ConnectionServices.SignalR
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public class PicturePathSignalR : IPicturePathRPC
    {
        private readonly HubConnection _connection;
        private readonly ILogger<PicturePathSignalR> _logger;

        public PicturePathSignalR(ILogger<PicturePathSignalR> logger, ApplicationSettings settings)
        {
            _connection = new HubConnectionBuilder()
                .WithUrl(settings.SignalRUrl)
                .Build();

            _logger = logger;
        }

        public async Task<Dictionary<Guid, string>> Get(IEnumerable<Guid> guidPicAr)
        {
            await StartConnection("SendGuids", new List<Guid>(guidPicAr));

            var answerReceived = false;
            var errorReceived = false;
            var newMessage = "ничего не пришло";
            var returnedDict = new Dictionary<Guid, string>();
            _connection.On<Dictionary<Guid, string>>("ReceiveGuidsPaths", (dictGuidsPaths) =>
            {
                returnedDict = dictGuidsPaths;
                answerReceived = true;
                _logger.LogInformation(newMessage);
            });

            var timer = 0;
            while (!answerReceived && !errorReceived && timer < 5)
            {
                Thread.Sleep(1000);
                timer++;
            }

            if (timer >= 5)
            {
                newMessage = "превышено время ожидания";
                _logger.LogError("превышено время ожидания");
            }

            await _connection.StopAsync();

            return returnedDict;
        }

        private async Task StartConnection(string methodName, object arg)
        {
            try
            {
                await _connection.StartAsync();
                _logger.LogInformation("Connection started");
                await _connection.SendAsync(methodName, arg);
                _logger.LogInformation("Message sended");
            }
            catch (Exception ex)
            {
                _logger.LogError("Error connection" + ex.Message);
                throw;
            }
        }
    }
}
