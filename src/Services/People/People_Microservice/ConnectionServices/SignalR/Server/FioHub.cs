﻿using Microsoft.AspNetCore.SignalR;
using Services.Abstractions;

namespace People_Microservice.ConnectionServices.SignalR.Server
{
    /// <summary>
    /// Hub for SignalR: отдать ФИО по Guid
    /// </summary>
    public class FioHub : Hub
    {
        private readonly ILogger<FioHub> _logger;
        private readonly IPersonService _personService;

        public FioHub(IPersonService personService, ILogger<FioHub> logger)
        {
            _personService = personService;
            _logger = logger;
        }

        public async Task SendGuids(List<Guid> message)
        {
            var outerDict = new Dictionary<Guid, string>();
            if (message != null && message.Any())
            {
                outerDict = await _personService.GetFioByGuidArray(message);
            }

            await this.Clients.All.SendAsync("ReceiveGuidsFios", outerDict);
        }

    }
}
