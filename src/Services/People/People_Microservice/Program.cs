using AutoMapper;
using Infrastructure.EntityFramework;
using Infrastructure.Repositories.Implementations;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using People_Microservice.Middlewares;
using People_Microservice.Settings;
using Services.Abstractions;
using Services.Implementations;
using Services.Repositories.Abstractions;
using SharedBus.Options;
using People_Microservice.ConnectionServices.Abstractions;
using People_Microservice.ConnectionServices.SignalR;
using People_Microservice.ConnectionServices.SignalR.Server;

var builder = WebApplication.CreateBuilder(args);

string origin = "FioOrigin";
builder.Services.AddCors(o => o.AddPolicy(origin, builder =>
{
    builder.AllowAnyOrigin()
        .AllowAnyMethod()
        .AllowAnyHeader();
}));

builder.Services.AddSignalR();

// Add services to the container.
builder.Services.AddSingleton<IMapper>(new Mapper(MapperSettings.GetMapperConfiguration()));
builder.Services.AddControllers();

var configuration = builder.Configuration;
var applicationSettings = configuration.Get<ApplicationSettings>();

var rabbitSettings = configuration.GetSection("RabbitMQ");
var options = rabbitSettings.Get<RabbitSettings>()!;

builder.Services.AddOptions<RabbitMqTransportOptions>()
    .Configure(o =>
    {
        o.Host = options.Server;
        o.VHost = options.VirtualHost;
        o.User = options.User;
        o.Pass = options.Password;
        // configure options manually, but usually bind them to a configuration section
    });
builder.Services.AddMassTransit(x =>
{
    x.UsingRabbitMq();
});

builder.Services.AddTransient<IPicturePathRPC, PicturePathSignalR>();

builder.Services.AddSingleton(applicationSettings);

builder.Services.AddScoped(typeof(IRepository<>), typeof(Repository<>));

builder.Services.AddTransient<IPersonRepository, PersonRepository>();
builder.Services.AddTransient<IPersonService, PersonService>();

builder.Services.AddTransient<IRelationshipRepository, RelationshipRepository>();
builder.Services.AddTransient<IRelationshipTypeRepository, RelationshipTypeRepository>();
builder.Services.AddTransient<IRelationsService, RelationsService>();

builder.Services.AddTransient<IFamilyRepository, FamilyRepository>();
builder.Services.AddTransient<IFamilyService, FamilyService>();

builder.Services.AddTransient<IPersonFamilyConnectService, PersonFamilyConnectService>(); 
builder.Services.AddScoped<IDbCreator, DbCreator>();

builder.Services.AddDbContext<DatabaseContext>(optionsBuilder =>
{
    optionsBuilder.UseNpgsql(builder.Configuration.GetConnectionString("DefaultConnection"));
    //optionsBuilder.UseSqlite(applicationSettings.ConnectionString);
});


// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

app.UseCors(x => x
    .AllowAnyMethod()
    .AllowAnyHeader()
    .SetIsOriginAllowed(origin => true)
    .AllowCredentials());

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();
app.UseHealthChecks("/health");
app.UseMiddleware<IncomingRequestsLoggingMiddleware>();

using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;
    
    var dbCreator = services.GetService<IDbCreator>();
    dbCreator?.Create();
}

app.MapHub<FioHub>("/fiopath");   // PhotoPathHub ����� ������������ ������� �� ���� /photopath

app.Run();
