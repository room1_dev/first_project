﻿using AutoMapper;
using People_Microservice.Mapping;

namespace People_Microservice.Settings
{
    public static class MapperSettings
    {
        public static MapperConfiguration GetMapperConfiguration()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<PersonMappingProfile>();
                cfg.AddProfile<FamilyMappingProfile>();
                cfg.AddProfile<RelationMappingProfile>();
                cfg.AddProfile<Services.Implementations.Mapping.PersonMappingsProfile>();
                cfg.AddProfile<Services.Implementations.Mapping.FamilyMappingsProfile>();
            });
            configuration.AssertConfigurationIsValid();
            return configuration;
        }
    }
}
