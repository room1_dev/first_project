﻿namespace People_Microservice.Settings
{
    public class ApplicationSettings
    {
        public string ConnectionString { get; set; } = string.Empty;
        public string GrpcServerAdress { get; set; } = string.Empty;
        public string SignalRUrl { get; set; } = string.Empty;
    }
}
