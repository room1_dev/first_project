﻿using AutoMapper;
using People_Microservice.Models.Person;
using Services.Contracts.Persons;

namespace People_Microservice.Mapping
{
    public class PersonMappingProfile : Profile
    {
        public PersonMappingProfile()
        {
            CreateMap<PersonDTO, PersonModel>();
            CreateMap<CreateUpdatePersonModel, CreateUpdatePersonDTO>();
        }
    }
}
