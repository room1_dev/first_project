﻿using AutoMapper;
using People_Microservice.Models.Family;
using Services.Contracts.Families;

namespace People_Microservice.Mapping
{
    public class FamilyMappingProfile : Profile
    {
        public FamilyMappingProfile()
        {
            CreateMap<FamilyDTO, FamilyModel>();
            CreateMap<FamilyDTO, SimpleFamilyModel>();
            CreateMap<CreateUpdateFamilyModel, CreateUpdateFamilyDTO>();
        }
    }
}
