﻿using AutoMapper;
using People_Microservice.Models.RelationModel;
using Services.Contracts.Relations;
using SharedBus;

namespace People_Microservice.Mapping
{
    public class RelationMappingProfile : Profile
    {
        public RelationMappingProfile() 
        {
            CreateMap<PersonWithRelationsSubDTO, PersonWithRelationsSubModel>();
            CreateMap<PersonWithRelationsDTO, PersonWithRelationsModel>();
            
            CreateMap<RelationRequestDTO, RelationRequestModel>()
                .ForMember(m => m.RelationType, map => map.Ignore());

            CreateMap<RelationRequestDTO, PersonWithRelationsSubModel>()
                .ForMember(m => m.RelationType, map => map.Ignore());

            CreateMap<RelationForTreeDTO, RelationForTreeModel>()
                .ForMember(m => m.Image, map => map.Ignore());

            CreateMap<RelationForTreeDTO, RelationForPassportModel>()
                .ForMember(m => m.Image, map => map.Ignore());

            CreateMap<CreateRelationModel, CreateRelationDTO>();
        }
    }
}
