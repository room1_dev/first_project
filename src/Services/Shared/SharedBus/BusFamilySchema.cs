﻿namespace SharedBus;

public class BusFamilySchema
{
    public Guid FamilyId { get; set; }
    public RelationForTreeModel[] Result { get; set; }
}