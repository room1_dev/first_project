using System.ComponentModel;

namespace SharedBus.Enum;

public enum RelationTypeModel
{
    ///<summary>Неопределено</summary>
    [Description("")]
    Unknown = 0, 
    
    ///<summary>Кровное родство</summary>
    [Description("Blood")]
    Blood = 1, 
    
    ///<summary>В браке</summary>
    [Description("Married")]
    Married = 2,
    
    ///<summary>В разводе</summary>
    [Description("Divorced")]
    Divorced = 3
}