using SharedBus.Enum;

namespace SharedBus;

public class RelationRequestModel
{
    public Guid PersonGuid { get; set; }
    public RelationTypeModel RelationType { get; set; } = RelationTypeModel.Blood;
}