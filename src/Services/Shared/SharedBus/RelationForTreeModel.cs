namespace SharedBus;

public class RelationForTreeModel
{
    public Guid PersonGuid { get; set; }

    public string Name { get; set; }

    public int Gender { get; set; }

    public DateTime DateOfBirth { get; set; }

    public RelationRequestModel[] Parents { get; set; }

    public RelationRequestModel[] Siblings { get; set; }

    public RelationRequestModel[] Spouses { get; set; }

    public RelationRequestModel[] Children { get; set; }
    public string Image { get; set; } = "https://www.kindpng.com/picc/m/256-2560186_user-name-icon-hd-png-download.png";

    /// <summary>
    /// ID �������� ��� �������
    /// </summary>
    public Guid? ImageId { get; set; } = null;
}