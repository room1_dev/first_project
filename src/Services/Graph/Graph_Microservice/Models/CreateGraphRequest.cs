namespace Graph_Microservice.Models;

/// <summary>
/// Запрос для генерации графа
/// </summary>
public class CreateGraphRequest 
{
    /// <summary>
    /// Идентификатор семьи
    /// </summary>
    public Guid FamilyId { get; set; }
        
    /// <summary>
    /// Моделей описывающая члена семьи и его семейные связи
    /// </summary>
    public string PersonRequestJsonModel { get; set; }
}