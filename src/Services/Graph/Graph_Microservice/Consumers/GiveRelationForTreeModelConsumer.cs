﻿using Application.Commands.CreateGraph;
using Graph_Microservice.Hubs;
using MassTransit;
using MediatR;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using SharedBus;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace Graph_Microservice.Consumers
{
    public class GiveRelationForTreeModelConsumer : IConsumer<BusFamilySchema>
    {
        private readonly IHubContext<MessageHub> _messageHub;
        private IMediator _mediator;
        
        public GiveRelationForTreeModelConsumer(IHubContext<MessageHub> messageHub, IMediator mediator)
        {
            _messageHub = messageHub;
            _mediator = mediator;
        }


        public async Task Consume(ConsumeContext<BusFamilySchema> context)
        {
            var message = JsonSerializer.Serialize(context.Message);
            var busFamilySchema = JsonConvert.DeserializeObject<BusFamilySchema>(message);
            var command = new CreateGraph.Command
            {
                FamilyId = busFamilySchema.FamilyId,
                PersonRequestModels = busFamilySchema.Result
            };
            
            await _mediator.Send(command);
            await _messageHub.Clients.All.SendAsync("sendToReact", "Схема дерева для семьи сгенерирована");
        }
    }
}
