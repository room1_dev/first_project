using Application;
using Graph_Microservice.Consumers;
using Graph_Microservice.Hubs;
using Graph_Microservice.Middlewares;
using Graph_Microservice.Settings;
using Infrastructure;
using Infrastructure.Abstractions.Repositories;
using Infrastructure.Repositories;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using SharedBus.Options;


string origin = "MyAllowSpecificOrigins";
var builder = WebApplication.CreateBuilder(args);

var configuration = builder.Configuration;
var rabbitSettings = configuration.GetSection("RabbitMQ");
var options = rabbitSettings.Get<RabbitSettings>()!;
var applicationSettings = configuration.Get<ApplicationSettings>();

builder.Services.AddOptions<RabbitMqTransportOptions>()
    .Configure(o =>
    {
        o.Host = options.Server;
        o.VHost = options.VirtualHost;
        o.User = options.User;
        o.Pass = options.Password;
        // configure options manually, but usually bind them to a configuration section
    });

builder.Services.AddMassTransit(cfg =>
{
    cfg.AddConsumer<GiveRelationForTreeModelConsumer>();
    cfg.UsingRabbitMq((context, cfg) =>
    {

        cfg.ConfigureEndpoints(context);
    });
});

builder.Services.AddControllers();
builder.Services.AddSignalR();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
builder.Services.AddDbContext<DataContext>(optionsBuilder =>
{
    optionsBuilder.UseNpgsql(applicationSettings.ConnectionString);
});

builder.Services.AddApplication();


builder.Services.AddCors(o => o.AddPolicy(origin, builder =>
{
    builder.AllowAnyOrigin()
        .AllowAnyMethod()
        .AllowAnyHeader();
}));

var app = builder.Build();
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseCors(x => x
    .AllowAnyMethod()
    .AllowAnyHeader()
    .SetIsOriginAllowed(origin => true)
    .AllowCredentials());

app.UseAuthorization();

app.MapControllers();
app.MapHub<MessageHub>("/message");

app.UseHealthChecks("/health");
app.UseMiddleware<IncomingRequestsLoggingMiddleware>();

using (var scope = app.Services.CreateScope())
{
    var serviceProvider = scope.ServiceProvider;

    var context =
        serviceProvider.GetRequiredService<DataContext>();
    DbInitializer.Initialize(context);
}


app.Run();