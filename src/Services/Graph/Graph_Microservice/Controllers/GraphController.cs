using Application.Commands.CreateGraph;
using Application.Models;
using Application.Queries.GetFamilyGraph;
using Graph_Microservice.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Graph_Microservice.Controllers;

/// <summary>
/// Граф
/// </summary>
[ApiController]
[Route("api/v1/[controller]")]
public class GraphController
    : BaseController
{
    /// <summary>
    /// Получить схему дерева по id семьи
    /// </summary>
    /// <param name="familyId">Id семьи, например <example>a61a79b9-a8ce-40bf-93dd-ce613c6f11ae</example></param>
    /// <returns></returns>
    [HttpGet("{familyId:guid}")]
    public Task<FamilyGraphResponse> GetGraphAsync(Guid familyId)
        => Mediator.Send(new GetFamilyGraph.Query{FamilyId = familyId});
}