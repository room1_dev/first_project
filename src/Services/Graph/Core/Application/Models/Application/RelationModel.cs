using System.Text.Json.Serialization;

namespace Application.Models.Application;

public class RelationModel
{
    [JsonPropertyName("id")]
    public Guid PersonGuid { get; set; }
    
    [JsonPropertyName("type")]
    public RelationTypeModel RelationType { get; set; }
}