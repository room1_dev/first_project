using System.ComponentModel;
using System.Text.Json.Serialization;
using Domain.Entities.Enums;

namespace Application.Models.Application;

public class JsonSchemaPersonModel
{
    [JsonPropertyName("id")]
    public Guid PersonGuid { get; set; }
    
    [JsonPropertyName("name")]
    public string Name { get; set; }
    
    [JsonPropertyName("img")]
    public string Image { get; set; }
    
    [JsonPropertyName("gender")]
    public Gender Gender { get; set; }
    
    [JsonPropertyName("parents")]
    public List<RelationModel> Parents { get; set; }
    
    [JsonPropertyName("siblings")]
    public List<RelationModel> Siblings { get; set; }
    
    [JsonPropertyName("spouses")]
    public List<RelationModel> Spouses { get; set; }
    
    [JsonPropertyName("children")]
    public List<RelationModel> Children { get; set; }
}