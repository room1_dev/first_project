using Application.Models;
using Application.Models.Application;
using AutoMapper;
using SharedBus;

namespace Application.MapProfiles.Application;

public class PersonRequestModelProfile : Profile
{
    public PersonRequestModelProfile()
    {
        CreateMap<RelationForTreeModel, JsonSchemaPersonModel>();
    }
}