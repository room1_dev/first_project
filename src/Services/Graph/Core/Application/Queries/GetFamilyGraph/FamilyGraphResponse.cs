using Application.Extensions;
using Application.Models;
using SharedBus;

namespace Application.Queries.GetFamilyGraph;

public class NodeModel
{
    public NodeModel(RelationForTreeModel model)
    {
        Id = model.PersonGuid.ToString();
        Img = model.Image;
        Name = model.Name;
        Parents = model.Parents.Select(p => new RelationModel(p)).ToArray();
        Siblings = model.Siblings.Select(p => new RelationModel(p)).ToArray();
        Spouses = model.Spouses.Select(p => new RelationModel(p)).ToArray();
        Children = model.Children.Select(p => new RelationModel(p)).ToArray();
    }

    /// <summary>
    /// Id
    /// </summary>
    public string Id { get; set; }
    
    public string Img { get; set; }
    
    public string Name { get; set; }

    ///<summary>Родители</summary>
    public RelationModel[] Parents { get; set; }
    
    ///<summary>Братья/сестры</summary>
    public RelationModel[] Siblings { get; set; }
    
    ///<summary>Супруги</summary>
    public RelationModel[] Spouses { get; set; }
    
    ///<summary>Дети</summary>
    public RelationModel[] Children { get; set; }
}

public class RelationModel
{
    public RelationModel(RelationRequestModel model)
    {
        Id = model.PersonGuid.ToString();
        Type = model.RelationType.GetDescription();
    }

    public string Id { get; set; }
    
    public string Type { get; set; }
}

/// <summary>
/// Модель ответа для запроса схемы дерева семьи
/// </summary>
public class FamilyGraphResponse
{
    /// <summary>
    /// Идентификатор семьи
    /// </summary>
    public Guid FamilyId { get; set; }
    
    /// <summary>
    /// Схема дерева
    /// </summary>
    public string? Graph { get; set; }
    
    public NodeModel[] NodeModelList { get; set; }
}