using MediatR;

namespace Application.Queries.GetFamilyGraph;

public partial class GetFamilyGraph
{
    /// <summary>
    /// Команда для запроса схемы дерева семьи
    /// </summary>
    public class Query : IRequest<FamilyGraphResponse>
    {
        /// <summary>
        /// Идентификатор семьи
        /// </summary>
        public Guid FamilyId { get; set; }
    }
}