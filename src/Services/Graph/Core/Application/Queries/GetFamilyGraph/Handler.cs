using Application.Models;
using Domain.Entities.Entities;
using Infrastructure.Abstractions.Repositories;
using MediatR;
using Newtonsoft.Json;
using SharedBus;

namespace Application.Queries.GetFamilyGraph;

public partial class GetFamilyGraph
{
    public class Handler : IRequestHandler<Query, FamilyGraphResponse>
    {
        private readonly IRepository<Graph> _graphRepository;

        public Handler(IRepository<Graph> graphRepository)
        {
            _graphRepository = graphRepository;
        }
        
        public async Task<FamilyGraphResponse> Handle(Query query, CancellationToken cancellationToken)
        {
            var graph =  await _graphRepository.Get(p=>p.FamilyGraph.FamilyId == query.FamilyId && p.IsLatest, cancellationToken);
            var schema = JsonConvert.DeserializeObject<RelationForTreeModel[]>(graph?.Schema);
            
            var result =  new FamilyGraphResponse
            {
                FamilyId = query.FamilyId,
                Graph = graph?.Schema,
                NodeModelList = schema.Select(p=> new NodeModel(p)).ToArray()
            };

            return result;
        }
    }
}