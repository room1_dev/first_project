using Application.Models;
using AutoMapper;
using Domain.Entities.Entities;
using Infrastructure.Abstractions.Repositories;
using MediatR;
using SharedBus;
using static System.DateTimeOffset;
using static Newtonsoft.Json.JsonConvert;

namespace Application.Commands.CreateGraph;

public partial class CreateGraph
{
    public class Handler : IRequestHandler<Command>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Graph> _graphRepository;
        private readonly IRepository<FamilyGraph> _familyGraphRepository;

        public Handler(IMapper mapper, IRepository<Graph> graphRepository, IRepository<FamilyGraph> familyGraphRepository)
        {
            _mapper = mapper;
            _graphRepository = graphRepository;
            _familyGraphRepository = familyGraphRepository;
        }
        
        public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
        {
            var relationModel = request.PersonRequestModels;
            var familyId = request.FamilyId;
            
            var relationModelList = _mapper.Map<RelationForTreeModel[]>(relationModel);
            var schema = SerializeObject(relationModelList);
            await AddOrUpdateFamilyGraphAsync(familyId, schema, cancellationToken);
            
            return await Unit.Task;
        }

        private async Task AddOrUpdateFamilyGraphAsync(Guid familyId, string schema, CancellationToken cancellationToken)
        {
            try
            {
                var familyGraphGuid = await GetOrCreateFamilyGraphAsync(familyId, cancellationToken);
                
                var latestFamilyGraph =
                    await _graphRepository.Get(p => p.FamilyGraphId == familyGraphGuid, cancellationToken);

                if (latestFamilyGraph != null)
                {
                    if (latestFamilyGraph.Schema == schema)
                    {
                        return;
                    }

                    latestFamilyGraph.IsLatest = false;
                    latestFamilyGraph.DateUpdated = DateTime.UtcNow; 
                    _graphRepository.Update(latestFamilyGraph);
                }
                
                await _graphRepository.AddAsync(
                    new Graph(familyGraphId: familyGraphGuid, schema: schema, dateCreated: Now.UtcDateTime,
                        isLatest: true), cancellationToken);

                await _graphRepository.SaveChangesAsync(cancellationToken);

            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        private async Task<Guid> GetOrCreateFamilyGraphAsync(Guid familyId, CancellationToken cancellationToken)
        {
            var familyGraph = await _familyGraphRepository.Get(p => p.FamilyId == familyId, cancellationToken);
            var familyGraphGuid = familyGraph?.Id ?? await _familyGraphRepository.AddAsync(
                new FamilyGraph(familyId: familyId), cancellationToken);

            return familyGraphGuid;
        }
    }
}