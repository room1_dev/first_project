using Application.Models;
using MediatR;
using SharedBus;

namespace Application.Commands.CreateGraph;

public partial class CreateGraph
{
    /// <summary>
    /// Команда для генерации графа
    /// </summary>
    public class Command : IRequest
    {
        /// <summary>
        /// Идентификатор семьи
        /// </summary>
        public Guid FamilyId { get; set; }
        
        /// <summary>
        /// Список моделей описывающий члена семьи и его семейные связи
        /// </summary>
        /// 
        public RelationForTreeModel[] PersonRequestModels { get; set; }
    }
}