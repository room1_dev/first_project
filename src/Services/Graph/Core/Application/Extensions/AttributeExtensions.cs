using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace Application.Extensions;

public static class AttributeExtensions
{
    public static string GetDescription(this object obj, string propertyName)
    {
        var property = obj.GetType().GetProperty(propertyName);
        return property != null
            ? property!.GetDescription()
            : propertyName;
    }

    public static string GetDescription(this MemberInfo property) => property?.GetCustomAttribute<DisplayAttribute>()?.GetDescription() ?? property?.Name;

    public static string GetDescription<TEnum>(this TEnum enumValue)
        where TEnum : struct, Enum
        => GetDescription(enumValue.AsNullable());
    
    public static T? AsNullable<T>(this T data)
        where T : struct
        => data;
    
    public static string GetDescription<TEnum>(this TEnum? enumValue)
        where TEnum : struct, Enum
        => enumValue.HasValue
            ? enumValue.GetType().GetField(enumValue.ToString()!).GetDescription()
            : "";
}