namespace Domain.Entities.Contracts;

public class BaseEntity
{
    public Guid Id { get; set; }
}