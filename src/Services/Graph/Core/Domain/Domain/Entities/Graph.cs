using Domain.Entities.Contracts;

namespace Domain.Entities.Entities;

public class Graph : BaseEntity
{
    public Graph()
    {
    }

    public Graph(Guid familyGraphId, string schema, DateTimeOffset dateCreated, bool isLatest)
    {
        FamilyGraphId = familyGraphId;
        Schema = schema;
        DateCreated = dateCreated;
        IsLatest = isLatest;
    }

    public string Schema { get; set; }
    
    public bool IsLatest { get; set; }
    
    public DateTimeOffset DateCreated { get; set; }
    
    public DateTimeOffset? DateUpdated { get; set; }
    
    public Guid FamilyGraphId { get; set; }

    public virtual FamilyGraph FamilyGraph { get; set; }
    
}