﻿using Domain.Entities.Contracts;
namespace Domain.Entities.Entities;

public class FamilyGraph : BaseEntity
{
    public FamilyGraph()
    {
    }

    public FamilyGraph(Guid familyId)
    {
        FamilyId = familyId;
    }

    public Guid FamilyId { get; set; }
    
    public virtual ICollection<Graph> Graphs { get; set; } = new List<Graph>();
}