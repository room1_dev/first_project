namespace Domain.Entities.Enums;

/// <summary>
/// Пол
/// </summary>
public enum Gender
{
    ///<summary>Неопределено</summary>
    Unknown = 0,
    
    ///<summary>Мужской</summary>
    Male = 1,
    
    ///<summary>Женский</summary>
    Female = 2
}