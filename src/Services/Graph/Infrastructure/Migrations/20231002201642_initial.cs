﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class initial : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FamilyGraphs",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    FamilyId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FamilyGraphs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Graphs",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Schema = table.Column<string>(type: "text", nullable: false),
                    IsLatest = table.Column<bool>(type: "boolean", nullable: false),
                    DateCreated = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DateUpdated = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    FamilyGraphId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Graphs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Graphs_FamilyGraphs_FamilyGraphId",
                        column: x => x.FamilyGraphId,
                        principalTable: "FamilyGraphs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Graphs_FamilyGraphId",
                table: "Graphs",
                column: "FamilyGraphId");
            
            migrationBuilder.InsertData(
                table: "FamilyGraphs",
                columns: new[] { "Id", "FamilyId" },
                values: new object[,]
                {
                    { Guid.Parse("6B29FC40-CA47-1067-B31D-00DD010662DA"), Guid.NewGuid() },
                });
            
            migrationBuilder.InsertData(
                table: "Graphs",
                columns: new[] { "Id", "Schema", "IsLatest", "DateCreated", "DateUpdated", "FamilyGraphId" },
                values: new object[,]
                {
                    { Guid.NewGuid(), "test", true, DateTimeOffset.Now, null, Guid.Parse("6B29FC40-CA47-1067-B31D-00DD010662DA")}
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Graphs");

            migrationBuilder.DropTable(
                name: "FamilyGraphs");
        }
    }
}
