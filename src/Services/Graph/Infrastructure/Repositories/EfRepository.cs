using System.Linq.Expressions;
using Domain.Entities;
using Domain.Entities.Contracts;
using Infrastructure.Abstractions.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories;

public class EfRepository<TEntity>
    : IRepository<TEntity> 
    where TEntity : BaseEntity
{
    private DataContext DbContext { get; }

    public EfRepository(DataContext dbContext)
    {
        DbContext = dbContext;
    }

    public Task<TEntity?> Get(Expression<Func<TEntity, bool>> expression, CancellationToken cancellationToken)
        =>  DbContext.Set<TEntity>().FirstOrDefaultAsync(expression, cancellationToken);

    public Task<TEntity?> GetByIdAsync(Guid id, CancellationToken cancellationToken)
        => DbContext.Set<TEntity>().FirstOrDefaultAsync(x => x.Id == id, cancellationToken);

    public async Task<Guid> AddAsync(TEntity item, CancellationToken cancellationToken)
    {
        await DbContext.AddAsync(item, cancellationToken);
        return item.Id;
    }

    public void Delete(TEntity item)
    {
        DbContext.Set<TEntity>().Remove(item);
    }

    public Task<IEnumerable<TEntity>> GetAllAsync()
        => Task.FromResult(DbContext.Set<TEntity>().AsEnumerable());


    public void Update(TEntity item)
        => DbContext.Entry(item).State = EntityState.Modified;
    
    public async Task SaveChangesAsync(CancellationToken cancellationToken = default)
    {
        await DbContext.SaveChangesAsync(cancellationToken);
    }
}