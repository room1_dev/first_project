using System.Linq.Expressions;
using Domain.Entities;
using Domain.Entities.Contracts;

namespace Infrastructure.Abstractions.Repositories;

public interface IRepository<T>
    where T: BaseEntity
{
    Task<IEnumerable<T>> GetAllAsync();
    
    Task<T?> Get(Expression<Func<T, bool>> expression, CancellationToken cancellationToken);
        
    Task<T?> GetByIdAsync(Guid id, CancellationToken cancellationToken);

    Task<Guid> AddAsync(T item, CancellationToken cancellationToken);

    void Delete(T item);

    void Update(T entity);

    Task SaveChangesAsync(CancellationToken token);
}