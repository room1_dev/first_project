﻿using Domain.Entities;
using Domain.Entities.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure;

public partial class DataContext : DbContext
{
    public DataContext(DbContextOptions<DataContext> options)
        : base(options)
    
    {
    }
    
    public DbSet<FamilyGraph> FamilyGraphs { get; set; }
    public DbSet<Graph> Graphs { get; set; }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(GetType().Assembly);
        base.OnModelCreating(modelBuilder);
    }
}