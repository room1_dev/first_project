using Domain.Entities;
using Domain.Entities.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityConfigurations;

public abstract class EntityConfigurationBase<TEntity> : IEntityTypeConfiguration<TEntity>
    where TEntity : BaseEntity
{
    public void Configure(EntityTypeBuilder<TEntity> builder)
    {
        builder.HasKey(p => p.Id);
    }
    protected virtual void ConfigureProperties(EntityTypeBuilder<TEntity> builder)
    {
    }

    protected virtual void ConfigureNavigationProperties(EntityTypeBuilder<TEntity> builder)
    {
    }

    protected virtual void ConfigureIndexes(EntityTypeBuilder<TEntity> builder)
    {
    }
}