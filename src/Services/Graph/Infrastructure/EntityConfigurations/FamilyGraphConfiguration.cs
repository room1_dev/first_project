using Domain.Entities;
using Domain.Entities.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityConfigurations;

public class FamilyGraphConfiguration : EntityConfigurationBase<FamilyGraph>
{
    protected override void ConfigureNavigationProperties(EntityTypeBuilder<FamilyGraph> builder)
    { 
        builder
            .HasMany(e => e.Graphs)
            .WithMany();
    }
}