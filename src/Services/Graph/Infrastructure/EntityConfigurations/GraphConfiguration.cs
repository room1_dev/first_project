using Domain.Entities;
using Domain.Entities.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityConfigurations;

public class GraphConfiguration  : EntityConfigurationBase<Graph>
{
    protected override void ConfigureNavigationProperties(EntityTypeBuilder<Graph> builder)
    {
        builder
            .HasOne(e => e.FamilyGraph)
            .WithMany()
            .HasForeignKey(e => e.FamilyGraphId);
    }
}