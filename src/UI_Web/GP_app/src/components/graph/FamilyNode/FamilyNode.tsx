import React, { useCallback } from 'react';
import classNames from 'classnames';
import css from './FamilyNode.module.css';
import ReactImageFallback from "react-image-fallback";
import { EextNode } from '../Graph/utils';

interface FamilyNodeProps {
  node: EextNode;
  isRoot: boolean;
  isHover?: boolean;
  onClick: (id: string) => void;
  onSubClick: (id: string) => void;
  style?: React.CSSProperties;
}

export const FamilyNode = React.memo(
  function FamilyNode({ node, isRoot, isHover, onClick, onSubClick, style }: FamilyNodeProps) {
    const clickHandler = useCallback(() => onClick(node.id), [node.id, onClick]);
    const clickSubHandler = useCallback(() => onSubClick(node.id), [node.id, onSubClick]);

    return (
      <div className={css.root} style={style}>
        <div
          className={classNames(
            css.inner,
            css[node.gender],
            isRoot && css.isRoot,
            isHover && css.isRoot,
          )}
          onClick={clickHandler}
        >
          <div className={css.content}>
            <div className={css.name}>{node.name}</div>
            <ReactImageFallback
                        src={node.img}
                        height={70}
                        fallbackImage="my-backup.png"
                        className={css.myimg} />
          </div>
                      
        </div>
        {node.hasSubTree && (
          <div
            className={classNames(css.sub, css[node.gender])}
            onClick={clickSubHandler}
          />
          
        )}
      </div>
    );
  },
);
