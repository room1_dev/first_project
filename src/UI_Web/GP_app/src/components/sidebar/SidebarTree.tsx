import * as React from 'react';
import { Link } from 'react-router-dom';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import Box from '@mui/material/Box';
import { ThemeProvider, createTheme } from '@mui/material/styles';

const buttons = [
  { key: "five", label: "Добавить родственника", link: "/view-family-description" },
  { key: "one", label: "Удалить родственника", link: "/add-person" },
  { key: "two", label: "Сохранить", link: "/Graph" },
];

const theme = createTheme({
  typography: {
    fontFamily: 'Acme', // Указываем шрифт Acme
  },
  palette: {
    primary: {
      main: '#000000', // Черный цвет
    },
  },
});

const SidebarTree: React.FC = () => {
  return (
    <>
      <div className='sidebar-page-grid-container'>
      <div className='sidebar-text' style={{ fontFamily: 'Acme', fontSize: '20px' }}>Возможности дерева</div>
        <ThemeProvider theme={theme}>
          <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100%', '& > *': { m: 1 } }}>
            <ButtonGroup orientation="vertical" aria-label="vertical outlined button group">
              {buttons.map((button) => (
                <Button key={button.key} component={Link} to={button.link} sx={{fontFamily: 'Acme'}}>
                  {button.label}
                </Button>
              ))}
            </ButtonGroup>
          </Box>
        </ThemeProvider>
      </div>
    </>
  );
};

export default SidebarTree;
