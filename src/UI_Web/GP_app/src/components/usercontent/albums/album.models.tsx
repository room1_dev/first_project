export interface Image {
    id: string
    name: string;
    source: string;
    albumId: string;
    url: string;
    imageText: string;
  }
  
  export interface Album {
    id: string
    name: string;
    images: Image[];
    userId: string;
  }
  