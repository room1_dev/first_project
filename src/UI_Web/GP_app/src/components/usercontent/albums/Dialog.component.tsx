import { Box, Button, Dialog, DialogActions, DialogContent, DialogTitle, Divider, TextField } from "@mui/material";
import { Dispatch, SetStateAction, useState } from "react";

interface Props {
    open: Dispatch<SetStateAction<boolean>>;
    OnHandleSubmit: () => void
    OnHandleCancel: () => void
}

export const PopupDialog = (props: Props) => {

    const [open, setOpen] = useState(false);
    // const handleClickOpen = () => {
    //     setAlbumName("");
    //     setOpen(true);
    // };

    const handleClose = () => {
        setOpen(false);
    };


    return (
        <Dialog open={open} onClose={handleClose} fullWidth maxWidth="sm">
                <DialogTitle>Новый альбом</DialogTitle>
                <Divider />
                <DialogContent >
                <Box component="form" sx={{'& .MuiTextField-root': { m: 1 }}} noValidate autoComplete="off">
                    {/* <TextField
                        name="familyName"
                        required
                        fullWidth
                        id="familyName"
                        label="Введите название альбома"
                         value={albumName}
                         onChange={handleAlbumNameChange}
                        autoFocus
                    /> */}
                </Box>
                </DialogContent>
                <DialogActions>
                <Button onClick={props.OnHandleSubmit}>Сохранить</Button>
                <Button onClick={props.OnHandleCancel}>Отмена</Button>
                </DialogActions>
            </Dialog>
    )
}