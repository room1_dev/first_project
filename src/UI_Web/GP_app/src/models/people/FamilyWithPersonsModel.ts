import { PersonForFamily } from "./PersonForFamily";

export interface FamilyWithPersonsModel {
    id: string;
    name: string;
    description: string;
    personList: PersonForFamily[];
}