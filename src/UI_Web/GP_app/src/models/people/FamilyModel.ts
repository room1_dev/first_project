export interface FamilyModel {
    id: string;
    name: string;
    description: string;
}