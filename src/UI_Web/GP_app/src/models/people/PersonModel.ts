export interface PersonModel {
    id: string;
    name: string;
}