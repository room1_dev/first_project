export interface ImagePersonModel {
    imageId: string;
    personId: string;
    fio: string;
}