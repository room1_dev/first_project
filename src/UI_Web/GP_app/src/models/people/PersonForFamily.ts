export interface PersonForFamily {
    id:string;
    firstName:string;
    middleName:string;
    lastName:string;
    dateOfBirth:string;
    dateOfDeath:string;
}