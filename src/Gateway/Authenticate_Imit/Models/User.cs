﻿namespace Authenticate_Imit.Models
{
    public record User(string Username, string Password, string Role, string[] Scopes);
}
