﻿namespace Authenticate_Imit.Models
{
    public record LoginModel(string Username, string Password);
}
