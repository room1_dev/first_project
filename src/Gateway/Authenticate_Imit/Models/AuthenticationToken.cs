﻿namespace Authenticate_Imit.Models
{
    public record AuthenticationToken(string Token, int ExpiresIn);
}
