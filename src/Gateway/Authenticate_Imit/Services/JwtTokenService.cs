﻿using Authenticate_Imit.Models;
using JwtTokenAuthentication;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Authenticate_Imit.Services
{
    public class JwtTokenService
    {
        private readonly List<User> _users = new()
    {
        new("admin", "aDm1n", "Administrator", new[] {"shoes.read"}),
        new("user01", "u$3r01", "User", new[] {"shoes.read"})
    };
        public AuthenticationToken? GenerateAuthToken(LoginModel loginModel)
        {
            //var tokenHandler = new JwtSecurityTokenHandler();
            //var key = Encoding.ASCII.GetBytes("G3VF4C6KFV43JH6GKCDFGJH45V36JHGV3H4C6F3GJC63HG45GH6V345GHHJ4623FJL3HCVMO1P23PZ07W8");
            //var issuer = "arbems.com";
            //var audience = "Public";
            //var claims = new List<Claim>
            //{
            //    new Claim(ClaimTypes.Name, userName),
            //    new Claim("UserType", userType)
            //};

            //var tokenDescriptor = new SecurityTokenDescriptor
            //{
            //    Issuer = issuer,
            //    Audience = audience,
            //    Subject = new ClaimsIdentity(claims.ToArray()),
            //    Expires = DateTime.UtcNow.AddDays(7),
            //    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            //};

            //var token = tokenHandler.CreateToken(tokenDescriptor);

            var user = _users.FirstOrDefault(u => u.Username == loginModel.Username
                                               && u.Password == loginModel.Password);
            if (user is null)
            {
                return null;
            }
            var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(JwtExtensions.SecurityKey));
            var signingCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);
            
            var expirationTimeStamp = DateTime.Now.AddSeconds(60);
            
            var claims = new List<Claim>
        {
            new Claim(JwtRegisteredClaimNames.Name, user.Username),
            new Claim("role", user.Role),
            new Claim("scope", string.Join(" ", user.Scopes))
        };
            var tokenOptions = new JwtSecurityToken(
                issuer: "https://localhost:7013",
                claims: claims,
                expires: expirationTimeStamp,
                signingCredentials: signingCredentials
            );
            var tokenString = new JwtSecurityTokenHandler().WriteToken(tokenOptions);
            return new AuthenticationToken(tokenString, (int)expirationTimeStamp.Subtract(DateTime.Now).TotalSeconds);
        }
    }
}
